/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application.stat;

/**
 *
 * @author Jaroslav
 */
public interface MinMaxElement<V> {

    public V getElement();

    public double get();

    public boolean add(double x, V element);

}
