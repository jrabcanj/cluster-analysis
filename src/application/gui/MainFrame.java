/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application.gui;

import java.awt.BorderLayout;
import java.awt.Container;
import javax.swing.JFrame;

/**
 *
 * @author Jaroslav
 */
public class MainFrame extends JFrame {
    
    private LoadPanel loadPanel;
    private InfoPanel infoPanel;
    private MainPanel mainPanel;
    
    public MainFrame(String title) {
        super(title);
        
        // Set layout manager
        setLayout(new BorderLayout());
        
        // Create Swing component
        loadPanel = new LoadPanel();
        infoPanel = new InfoPanel();
        mainPanel = new MainPanel();
        
        // Add Swing components to content pane
        Container c = getContentPane();
        
        c.add(loadPanel, BorderLayout.WEST);
        c.add(infoPanel, BorderLayout.CENTER);
        c.add(mainPanel, BorderLayout.EAST);
    }
}
