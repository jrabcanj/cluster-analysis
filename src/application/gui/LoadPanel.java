/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application.gui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.filechooser.FileSystemView;

/**
 *
 * @author Jaroslav
 */
public class LoadPanel extends JPanel {
    public LoadPanel() {
        Dimension size = getPreferredSize();
        size.width = 250;
        setPreferredSize(size);
        
        setBorder(BorderFactory.createTitledBorder("Load panel"));
        
        JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
        
        JButton btnFileChoose = new JButton("ChooseFile");
        btnFileChoose.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                jfc.showOpenDialog(null);
            }
        });
        
        setLayout(new GridBagLayout());
        
        GridBagConstraints gc = new GridBagConstraints();
        
        //// First column ////
        
        gc.anchor = GridBagConstraints.LINE_END;
        
        gc.gridx = 0;
        gc.gridy = 0;
        add(btnFileChoose, gc);        
    }
}
