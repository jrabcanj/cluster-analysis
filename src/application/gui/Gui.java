/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application.gui;
import javax.swing.*;

/**
 *
 * @author Jaroslav
 */
public class Gui{
    
    public static int guiWidth = 700;
    public static int guiHeight = 500;
    
    public static void main(String[] args) {
        
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                JFrame frame = new MainFrame("Clustering applictaion");
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setSize(guiWidth, guiHeight);
                frame.setVisible(true);
            }
        });
    }
}
