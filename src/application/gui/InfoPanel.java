/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application.gui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Jaroslav
 */
public class InfoPanel extends JPanel {
    public InfoPanel() {
        Dimension size = getPreferredSize();
        size.width = 250;
        setPreferredSize(size);
        
        setBorder(BorderFactory.createTitledBorder("Info panel"));
        
        JLabel labelLoad = new JLabel("Load data: ");
        JLabel labelSeparator = new JLabel("Separator: ");
        
        JTextField textFieldLoad = new JTextField(10);
        JTextField textFieldSeparator = new JTextField(10);
        
        JButton buttonLoad = new JButton("Load");
        
        setLayout(new GridBagLayout());
        
        GridBagConstraints gc = new GridBagConstraints();
        
        //// First column ////
        
        gc.anchor = GridBagConstraints.LINE_END;
        gc.weightx = 0.5;
        gc.weighty = 0.5;
        
        gc.gridx = 0;
        gc.gridy = 0;
        add(labelLoad, gc);
        
        gc.gridx = 0;
        gc.gridy = 1;
        add(labelSeparator, gc);
        
        //// Second column ////
        
        gc.anchor = GridBagConstraints.LINE_START;
        
        gc.gridx = 1;
        gc.gridy = 0;
        add(textFieldLoad, gc);        
        
        gc.gridx = 1;
        gc.gridy = 1;
        add(textFieldSeparator, gc);
        
        //// Final row ////
        gc.weighty = 10;
        
        gc.anchor = GridBagConstraints.FIRST_LINE_START;
        gc.gridx = 1;
        gc.gridy = 2;
        add(buttonLoad, gc);
    }
}