package application.chart;

import java.util.List;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;
 
/**
 *
 * @author Jaroslav
 */
 
public class ChartLine extends Application {
    
    public static double maxY = 0;
    private static int minY = 0;
    
    public static int width = 1200;
    public static int height = 700;
    public static int filter = 1;
    
    public static List<Double> distances;

    @Override public void start(Stage stage) {
        stage.setTitle("Clustering");
        
        final NumberAxis xAxis = new NumberAxis(0, distances.size(), 0.5);
        final NumberAxis yAxis = new NumberAxis(minY, maxY, 0.5);
        final LineChart<Number,Number> lineChart = new LineChart<Number,Number>(xAxis,yAxis);
        
        xAxis.setLabel("X coordinate");                
        yAxis.setLabel("Y coordinate");
        lineChart.setTitle("Clustering algorithm overview (line)");
        
        XYChart.Series series = new XYChart.Series();
        int counter = 0;
        for (Double dist : distances) {
            counter++;
            if (counter % filter == 1) {
                series.setName("Clusters");
                series.getData().add(new XYChart.Data(counter, dist));
            } else {
                continue;
            }
        }
        lineChart.getData().addAll(series);
        
        Scene scene = new Scene(lineChart, width, height);
        stage.setScene(scene);
        stage.show();
    }
 
    public static void main(String[] args) {
        launch(args);
    }
    
}