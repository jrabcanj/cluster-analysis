package application.chart;

import minig.clustering.GeneralCluster;
import java.util.List;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.ScatterChart;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;
import minig.clustering.Coordinates;

/**
 *
 * @author Jaroslav
 */
public class ChartPoint extends Application {
    
    private static int maxX = 1;
    private static int maxY = 1;
    
    private static int minX = Integer.MAX_VALUE;
    private static int minY = Integer.MAX_VALUE;
    
    public static int width = 900;
    public static int height = 700;
    
    public static List<GeneralCluster> clusters;

    private static void setZoom() {
        for (GeneralCluster cluster : clusters) {            
            for (Coordinates coor : cluster.getPointsCoordinates(width, height)) {
                if (coor.getX() > maxX)
                    maxX = coor.getX();
                if (coor.getY() > maxY)
                    maxY = coor.getY();
                
                if (coor.getX() < minX)
                    minX = coor.getX();
                if (coor.getY() < minY)
                    minY = coor.getY();
            }
        }
    }
    
    @Override public void start(Stage stage) {
        setZoom();
        stage.setTitle("Clustering");
        
        final NumberAxis xAxis = new NumberAxis(minX - 1, maxX + 1, maxX / 10);
        final NumberAxis yAxis = new NumberAxis(minY - 1, maxY + 1, maxY / 10);        
        final ScatterChart<Number,Number> sc = new ScatterChart<Number,Number>(xAxis,yAxis);
        
        xAxis.setLabel("X coordinate");                
        yAxis.setLabel("Y coordinate");
        sc.setTitle("Clustering algorithm overview (points)");
        
        int clusterNumber = 0;
        for (GeneralCluster cluster : clusters) {
            
            if (cluster.getPoints().size() == 1) continue;
            
            clusterNumber++;
            XYChart.Series series = new XYChart.Series();
            
            for (Coordinates coor : cluster.getPointsCoordinates(width, height)) {
                series.setName("Cluster c. " + clusterNumber);
                series.getData().add(new XYChart.Data(coor.getX(), coor.getY()));
            }
            sc.getData().addAll(series);
        }
            
        Scene scene  = new Scene(sc, width, height);
        stage.setScene(scene);
        stage.show();
    }
 
    public static void main(String[] args) {
        launch(args);
    }
    
}