/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application;

import application.structures.DoubleVector;
import application.structures.Vector;
import java.util.List;
import minig.data.attribute.AttrValue;

/**
 * @author Jaroslav
 */
public class BranchValues {

    private DoubleVector values;
    private int count;

    public BranchValues() {
    }

    public DoubleVector getValues() {
        return values;
    }

    public BranchValues(int size) {
        this.values = DoubleVector.ones(size);
    }

    public BranchValues getCopy() {
        return new BranchValues(this);
    }

    public int getSize() {
        if (values == null) {
            return 0;
        }
        return values.size();
    }

    public BranchValues(AttrValue val) {
        this.values = new DoubleVector(val.getValues());
    }

    public BranchValues(BranchValues sp) {
        copy(sp);
    }

    private void copy(BranchValues sp) {
        if (sp.values != null) {
            values = new DoubleVector(sp.values);
        }
        count = sp.count;
    }

    public BranchValues(List<Double> values) {
        this.values = new DoubleVector(values);
    }

    public void addSumproduct(BranchValues sp) {
        if (values == null) {
            values = new DoubleVector(sp.values);
        } else {
            values.muli(sp.values);
        }
    }

    public void addAttrVal(AttrValue val) {
        count++;
        if (values == null) {
            values = new DoubleVector(val.getValues());
        } else {
            values.muli(val.getValues());
        }
    }

    public void initByOnes(int count) {
        this.count = count;
        values = DoubleVector.ones(count);
    }

    public double getValueAtProductVector(int i) {
        return values.getNum(i);
    }

    @Override
    public String toString() {
        return values.toString();
    }

    public void print(int limit) {
        System.out.println(toString());
    }

    /**
     *
     * @return count of added attrValues
     */
    public int getCount() {
        return count;
    }

    public boolean isEmpty() {
        return values == null || values.isEmpty();
    }

    public double getSumProduct() {
        if (values == null) {
            return 0;
        }
        return values.sum();
    }

    public double getSumproductOf(AttrValue val) {
        Vector m = val.getValues();
        if (values == null) {
            return m.sum();
        }
        return values.mulsum(m);
        //return m.mulsum(values);
        // return m.mul(this.values).mulsum();
    }

    public double getSumproductOf(AttrValue val, AttrValue val1) {
        return this.values.mulmulsum(val.getValues(), val1.getValues());
    }

    public static double getSumProdOf(AttrValue val, AttrValue val1) {
        Vector m = val.getValues();
        Vector n = val1.getValues();
        return m.mulsum(n);
    }

    public double getProduct(Vector vaules) {
        if (values == null) {
            return ((DoubleVector) vaules).sum();
        } else {
            return values.mulsum(vaules);
        }
    }

    public void destroy() {
        values = null;
    }

}
