package application;

import application.structures.DoubleVector;
import application.structures.Vector;
import java.util.LinkedList;
import java.util.List;
import minig.data.attribute.AttrValue;

/**
 * @author Jaroslav
 */
public class BranchValuesSlow extends BranchValues {

    private LinkedList<Vector> vecors = new LinkedList();

    public BranchValuesSlow() {
    }

    public DoubleVector getValues() {
        return Vector.getProductVector(vecors);
    }

    public BranchValuesSlow(int size) {
    }

    public BranchValuesSlow getCopy() {
        return new BranchValuesSlow(this);
    }

    public int getSize() {
        if (vecors.isEmpty()) {
            return 0;
        }
        return vecors.getFirst().size();
    }

    public BranchValuesSlow(AttrValue val) {
        vecors.add(val.getValues());
    }

    public BranchValuesSlow(BranchValuesSlow sp) {
        vecors = new LinkedList<>(sp.vecors);
    }

    public BranchValuesSlow(List<Double> values) {
        this.vecors.add((DoubleVector) values);
    }

    public void addAttrVal(AttrValue val) {
        vecors.add(val.getValues());
    }

    public double getValueAtProductVector(int i) {
        double ret = 1;
        for (int j = vecors.size(); j > 0; j--) {
             ret *= vecors.get(j).get(i);
        }
        return ret;
    }

    @Override
    public String toString() {
        return vecors.toString();
    }

    public void print(int limit) {
        System.out.println(toString());
    }

    /**
     *
     * @return count of added attrValues
     */
    public int getCount() {
        return vecors.size();
    }

    public boolean isEmpty() {
        return vecors.isEmpty();
    }

    public double getSumProduct() {
        return Vector.sumproduct(vecors);
    }

    public LinkedList<Vector> getAllValues() {
        return vecors;
    }

    public double getSumproductOf(AttrValue val) {
        Vector m = val.getValues();
        if (vecors.isEmpty()) {
            return m.sum();
        }
        return Vector.sumproduct(vecors, m);
        // return m.mul(this.values).mulsum();
    }

    public double getSumproductOf(AttrValue val, AttrValue val1) {
        Vector m = val.getValues();
        Vector n = val1.getValues();
        return Vector.sumproduct(vecors, m, n);
    }

    public static double getSumProdOf(AttrValue val, AttrValue val1) {
        Vector m = val.getValues();
        Vector n = val1.getValues();
        return m.mulsum(n);
    }

    public double getProduct(List<Double> vaules) {
        if (vecors.isEmpty()) {
            return ((DoubleVector) vaules).sum();
        } else {
            return Vector.sumproduct(vecors, (DoubleVector) vaules);
        }
    }

    public void destroy() {
        vecors.clear();
    }
    
    

}
