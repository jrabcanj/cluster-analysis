/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minig.evaluation;

import java.util.ArrayList;
import minig.clustering.GeneralCluster;
import minig.data.dataset.DataSet;
import minig.data.dataset.instances.DatasetInstance;
import minig.distance.Distance;
import minig.distance.Euclidian;

/**
 *
 * @author Jaroslav
 */
public class CalinskyHarabaszIndex {
    
    private DataSet dataset;
    private Distance distance;
    private ArrayList<GeneralCluster> clusters;
    
    public CalinskyHarabaszIndex(DataSet dataSet) {
        dataset = dataSet;
        distance = new Euclidian(dataSet);
    }
    
    public double Measure(ArrayList<GeneralCluster> clusters) {
        double result = 0;
        this.clusters = clusters;
        if (clusters.size() == 1) return 0;
        return getNumberator() / getDenominator();
    }
    
    private double getNumberator() {
        double result = 0;
        int numOfClusters = clusters.size();
        double[] datasetCenteroid = GeneralCluster.getDatasetCentroid(dataset);
        for (GeneralCluster cluster : clusters) {
            int numOfPoints = cluster.getPoints().size();
            double dist = distance.getDistance(cluster.getCentroid(), datasetCenteroid);
            result += (numOfPoints * Math.pow(dist, 2));
        }
        return result / (numOfClusters - 1);
    }
    
    private double getDenominator() {
        double result = 0;
        int numOfObjects = dataset.getDatasetInstances().size();
        int numOfClusters = clusters.size();
        for (GeneralCluster cluster : clusters) {
            for (DatasetInstance point : cluster.getPoints()) {
                double dist = distance.getDistance(cluster.getCentroid(), point.getNumericData());
                result += Math.pow(dist, 2);
            }
        }
        return result / (numOfObjects - numOfClusters);
    }
}
