/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minig.evaluation;

import java.util.ArrayList;
import minig.clustering.GeneralCluster;
import minig.data.dataset.DataSet;
import minig.data.dataset.instances.DatasetInstance;
import minig.distance.Distance;
import minig.distance.Euclidian;

/**
 *
 * @author Jaroslav
 */
public class I_Index {
    
    private DataSet dataset;
    private Distance distance;
    private ArrayList<GeneralCluster> clusters;
    
    public I_Index(DataSet dataSet) {
        dataset = dataSet;
        distance = new Euclidian(dataSet);
    }
    
    public double Measure(ArrayList<GeneralCluster> clusters) {
        double result = 0;
        this.clusters = clusters;
        if (clusters.size() == 1) return 0;
        result = (1.0 / clusters.size()) * (getNumberator() / getDenominator()) * getMaxCentroidDist();
        return result;
    }
    
    private double getNumberator() {
        double result = 0;
        double[] datasetCenteroid = GeneralCluster.getDatasetCentroid(dataset);
        for (DatasetInstance point : dataset.getDatasetInstances()) {
            result += distance.getDistance(point.getNumericData(), datasetCenteroid);
        }
        return result;
    }
    
    private double getDenominator() {
        double result = 0;
        for (GeneralCluster cluster : clusters) {
            for (DatasetInstance point : cluster.getPoints()) {
                result += distance.getDistance(point, cluster.getCentroid());
            }
        }
        return result;
    }
    
    private double getMaxCentroidDist() {
        double result = Double.MIN_VALUE;
        for (GeneralCluster c1 : clusters) {
            for (GeneralCluster c2 : clusters) {
                double dist = distance.getDistance(c1.getCentroid(), c2.getCentroid());
                result = Math.max(dist, result);
            }
        }
        return result;
    }
}
