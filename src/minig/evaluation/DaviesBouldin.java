/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minig.evaluation;

import java.util.ArrayList;
import minig.clustering.GeneralCluster;
import minig.data.dataset.DataSet;
import minig.data.dataset.instances.DatasetInstance;
import minig.distance.Distance;
import minig.distance.Euclidian;

/**
 *
 * @author Jaroslav
 */
public class DaviesBouldin {
    
    private Distance distance;
    private ArrayList<GeneralCluster> clusters;
    
    public DaviesBouldin(DataSet dataSet) {
        distance = new Euclidian(dataSet);
    }
    
    public double Measure(ArrayList<GeneralCluster> clusters) {
        double result = 0;
        this.clusters = clusters;
        if (clusters.size() == 1) return 0;
        for (int i = 0; i < clusters.size(); i++) {
            result += getMaxFromFunction(i);
        }
        return result / clusters.size();
    }
    
    private double getAvgDistFromCentroid(GeneralCluster cluster) {
        double result = 0;
        double[] centroid = cluster.getCentroid();
        for (DatasetInstance point : cluster.getPoints()) {
            result += distance.getDistance(point.getNumericData(), centroid);
        }
        return result / cluster.getPoints().size();
    }
    
    private double getMaxFromFunction(int clusterIndex) {
        double result = Double.MIN_VALUE;
        for (int i = 0; i < clusters.size(); i++) {
            if (i == clusterIndex) continue;
            double centroidDistance = distance.getDistance(clusters.get(i).getCentroid(), clusters.get(clusterIndex).getCentroid());
            double res = (getAvgDistFromCentroid(clusters.get(i)) + getAvgDistFromCentroid(clusters.get(clusterIndex))) / centroidDistance;
            result = Math.max(result, res);
        }
        return result;
    }
}
