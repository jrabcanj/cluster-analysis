/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minig.evaluation;

import java.util.ArrayList;
import minig.clustering.GeneralCluster;
import minig.data.dataset.DataSet;
import minig.data.dataset.instances.DatasetInstance;
import minig.distance.Distance;
import minig.distance.Euclidian;

/**
 *
 * @author Jaroslav
 */
public class DunnIndex {
    
    private Distance distance;
    ArrayList<GeneralCluster> clusters;
    
    public DunnIndex(DataSet dataSet) {
        this.distance = new Euclidian(dataSet);
    }
    
    public double Measure(ArrayList<GeneralCluster> clusters) {
        this.clusters = clusters;
        if (clusters.size() == 1) return 0;
        
        double maxDistance = this.getMaxDistanceInCluster();
        double minDistance = this.getMinDistanceBetweenClusters();
        return minDistance / maxDistance;
    }
    
    private double getMaxDistanceInCluster() {
        double result = 0;
        for (int i = 0; i < this.clusters.size(); i++) {
            GeneralCluster cluster = this.clusters.get(i);
            for (int j = 0; j < cluster.getPoints().size() - 1; j++) {
                DatasetInstance p1 = cluster.getPoints().get(j);
                DatasetInstance p2 = cluster.getPoints().get(j + 1);
                double actDist = this.distance.getDistance(p1.getIndex(), p2.getIndex());
                if (actDist > result) {
                    result = actDist;
                }
            }
        }
        return result;
    }
    
    private double getMinDistanceBetweenClusters() {
        double result = Double.MAX_VALUE;
        for (int i = 0; i < this.clusters.size() - 1; i++) {
            GeneralCluster c1 = this.clusters.get(i);
            GeneralCluster c2 = this.clusters.get(i + 1);
            double dist = distance.getDistance(c1.getCentroid(), c2.getCentroid());
            if (dist < result) {
                result = dist;
            }
        }
        return result;
    }
}
