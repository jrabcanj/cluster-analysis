/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minig.data.attribute;

import application.structures.IntegerVector;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author Jaroslav
 */
public class LinguisticAttr extends CategoricalAttr<String> {

    private List<LingvisticValue> attrValues = new ArrayList();
    private IntegerVector values = new IntegerVector();
    public final int type = 2;

    public LinguisticAttr(String name, List<String> values) {
        super(name);
        for (String value : values) {
            addValue(value);
        }
    }

    public LinguisticAttr(String name) {
        super(name);
    }

    public LinguisticAttr(String name, String... classes) {
        super(name);
        this.attrValues = new ArrayList(classes.length);
        for (String ligValue : classes) {
            addAttrValue(ligValue);
        }
    }

    public LingvisticValue getAttrValue(int index) {
        return attrValues.get(index);
    }

    public String getAttrValueName(int i) {
        return attrValues.get(i).getName();
    }

    public String getDomainName(int domainindex) {
        return attrValues.get(domainindex).getName();
    }

    public int getAttrValueIndex(LingvisticValue value) {
        return attrValues.indexOf(value);
    }

    public void destroyData() {
        values.destroyData();
    }

    public LingvisticValue addAttrValue(String className) {
        final LingvisticValue lingvisticValue = new LingvisticValue(this, className);
        if (!this.attrValues.contains(lingvisticValue)) {
            this.attrValues.add(lingvisticValue);
            lingvisticValue.setValIndex(attrValues.size() - 1);
        }
        return lingvisticValue;
    }

    public void addAttrValues(String... className) {
        for (String string : className) {
            addAttrValue(string);
        }
    }

    @Override
    public LinguisticAttr getRawCopy() {
        LinguisticAttr a = getEmptyCopy();
        a.values = this.values;
        a.attrValues = this.attrValues;
        a.setAttributeIndex(getAttributeIndex());
        return a;
    }

    public void setAttributeIndex(int index) {
        super.setAttributeIndex(index);
        for (LingvisticValue attrValue : attrValues) {
            attrValue.setAttribute(this);
        }
    }

    @Override
    public List<LingvisticValue> getDomain() {
        return attrValues;
    }

    public int getDomainSize() {
        return attrValues.size();
    }

    @Override
    public List<String> getDomainNames() {
        return attrValues.stream().map((t) -> {
            return t.getName();
        }).collect(Collectors.toList());
    }

    public List<LingvisticValue> getClasses() {
        return attrValues;
    }

    public IntegerVector getValues() {
        return values;
    }

    @Override
    public final void addValue(String value) {
        LingvisticValue val = addAttrValue(value);
        int clasIndex = LinguisticAttr.this.getAttrValueIndex(val);
        values.add(clasIndex);
    }

    public FuzzyAttr transformToFuzzyAttribute() {
        FuzzyAttr a = new FuzzyAttr(getName());

        return a;
    }

    @Override
    public int getDataCount() {
        return values.size();
    }

    @Override
    public String getRowString(int i) {
        int clsIndex = values.getPoint(i);
        return attrValues.get(clsIndex).getName();
    }

    @Override
    public String getUnformatedRowString(int i) {
        return getRowString(i);
    }

    @Override
    public String getRow(int index) {
        int clsIndex = values.getPoint(index);
        return attrValues.get(clsIndex).getName();
    }

    @Override
    public int getClassIndex(int row) {
        return values.getPoint(row);
    }

    public int getAttrValueIndex(String attrValName) {
        for (LingvisticValue val : attrValues) {
            if (val.getName().equals(attrValName)) {
                return val.getValIndex();
            }
        }
        return -1;
    }

    @Override
    public void print() {
        String s = getName().toUpperCase() + "\n";
        StringBuilder sb = new StringBuilder();
        for (Integer value : values) {
            sb.append(attrValues.get(value)).append("\n");
        }
        System.out.println(s + sb.toString());
    }

    @Override
    public String toString() {
        return "Lingvistic{" + getName() + '}';
    }

    @Override
    public LinguisticAttr getEmptyCopy() {
        LinguisticAttr attr = new LinguisticAttr(getName());
        for (LingvisticValue attrValue : attrValues) {
            attr.addAttrValue(attrValue.getName());
        }
        attr.values = new IntegerVector();
        attr.setIsOutputAttr(false);
        return attr;
    }

    @Override
    public void addRow(String val) {
        this.addValue(val);
    }

    public void addRow(Integer classIndex) {
        if (classIndex > attrValues.size() - 1) {
            throw new Error("Unknow class index: " + classIndex + " for attribute: " + getName().toUpperCase() + ", during adding row");
        }
        values.add(classIndex);
    }

    public void addRows(String... val) {
        for (String string : val) {
            this.addRow(string);
        }
    }

    public void addRows(int... val) {
        for (int v : val) {
            addRow(v);
        }
    }

    @Override
    public int getType() {
        return Attribute.LINGVISTIC;
    }

    @Override
    public int getRowLength() {
        return 1;
    }

    @Override
    public void removeRow(int index) {
        values.remove(index);
    }

    public static void main(String[] args) {
        LinguisticAttr attr = new LinguisticAttr("AATR", "A", "B", "C");
        attr.addRows(0, 1, 1, 2, 1, 1, 1, 2);
        attr.print();
    }

}
