/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minig.data.attribute;

/**
 *
 * @author Jaroslav
 * @param <T>
 */
public abstract class CategoricalAttr<T> extends Attribute<T> {

    public CategoricalAttr(String name) {
        super(name);
    }

    public CategoricalAttr() {
        super();
    }

    public abstract int getDomainSize();

    public abstract int getClassIndex(int row);

    public abstract String getAttrValueName(int index);

}
