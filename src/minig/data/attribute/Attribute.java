/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minig.data.attribute;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import minig.data.dataset.DataSet;

/**
 *
 * @author Jaroslav
 * @param <T>
 */
public abstract class Attribute<T> implements Serializable {

    public final static int NUMERIC = 0;
    public final static int FUZZY = 1;
    public final static int LINGVISTIC = 2;

    private int attributeIndex = -1;
    private String name;
    private transient DataSet dataset;
    private boolean isOutput = false;

    public Attribute(String name) {
        this.name = name;
    }

    public Attribute() {
    }

    public abstract Attribute getRawCopy();

    public abstract Attribute getEmptyCopy();

    public abstract int getType();

    public String getName() {
        return name;
    }

    public static String getTypeName(Attribute attr) {
        switch (attr.getType()) {
            case NUMERIC:
                return "Numeric";
            case LINGVISTIC:
                return "Lingvistic";
            case FUZZY:
                return "Fuzzy";
            default:
                return attr.getClass().getSimpleName();
        }
    }

    public void setName(String name) {
        this.name = name;
    }

    public NumericAttr numeric() {
        return (NumericAttr) this;
    }

    public FuzzyAttr fuzzy() {
        return (FuzzyAttr) this;
    }

    public LinguisticAttr lingvistic() {
        return (LinguisticAttr) this;
    }

    public abstract void destroyData();
    
    public abstract void removeRow(int index);

    public abstract Object getValues();

    public abstract List<String> getDomainNames();

    public abstract Collection getDomain();

    public abstract int getRowLength();

    public abstract int getDataCount();

    public abstract void addValue(String valuesName);

    public abstract String getRowString(int i);

    public abstract String getUnformatedRowString(int i);

    public boolean isOutputAttr() {
        return isOutput;
    }

    public boolean isInputAttr() {
        return !isOutputAttr();
    }

    public void setIsOutputAttr(boolean isOutput) {
        this.isOutput = isOutput;
    }

    public abstract T getRow(int index);

    public abstract void addRow(T index);

    public void setDataset(DataSet dataSet) {
        this.dataset = dataSet;
    }

    public DataSet getDataset() {
        return dataset;
    }

    public void print() {
        System.out.println(toString());
    }

    public int getAttributeIndex() {
        return attributeIndex;
    }

    public void setAttributeIndex(int attributeIndex) {
        this.attributeIndex = attributeIndex;
    }

    public static boolean isNumeric(Attribute a) {
        return a.getType() == NUMERIC;
    }

    public static boolean isLingvistic(Attribute a) {
        return a.getType() == LINGVISTIC;
    }

    public static boolean isFuzzy(Attribute a) {
        return a.getType() == FUZZY;
    }

    public static boolean isCategorical(Attribute a) {
        return a.getType() == FUZZY || a.getType() == LINGVISTIC;
    }

    public boolean isNumeric() {
        return Attribute.isNumeric(this);
    }

    public boolean isCategorical() {
        return Attribute.isCategorical(this);
    }

    public boolean isFuzzy() {
        return Attribute.isFuzzy(this);
    }

    public boolean isLingvistic() {
        return Attribute.isLingvistic(this);
    }

    @Override
    public String toString() {
        return "name=" + name + getDomain().size();
    }

}
