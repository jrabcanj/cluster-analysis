/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minig.data.io.load;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.logging.Level;
import java.util.logging.Logger;
import minig.data.attribute.Attribute;
import minig.data.attribute.FuzzyAttr;
import minig.data.attribute.LinguisticAttr;
import minig.data.attribute.NumericAttr;
import minig.data.dataset.DataSet;

/**
 *
 * @author Jaroslav
 */
public class MyLoader extends DataLoader {

    public MyLoader(String path) {
        super(path);
    }

    public MyLoader(String path, String separator) {
        super(path, separator);
    }

    public MyLoader(DataSet dataset, String path, String separator) {
        super(dataset, path, separator);
    }

    @Override
    public void load() {
        BufferedReader br = null;
        try {
            final File file = new File(getPath());
            br = Files.newBufferedReader(file.toPath());
            String pom;
            getDataset().setName(file.getName());
            boolean attrsReaded = false;
            while ((pom = br.readLine()) != null) {
                if (!attrsReaded) {
                    if (pom.trim().equals("#VAL#")) {
                        attrsReaded = true;
                        continue;
                    }
                    String[] attributeInfo = pom.split(getSeparator());
                    Attribute a = getAttribute(attributeInfo);
                    getDataset().addAttribute(a);
                } else {
                    String[] row = pom.split(getSeparator());
                    addInstance(row);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(MyLoader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private <T extends Attribute> T getAttribute(String[] attributeInfo) {
        T attr;
        if (attributeInfo.length == 1) {
            String[] newattributeInfo = new String[2];
            newattributeInfo[0] = attributeInfo[0];
            newattributeInfo[1] = attributeInfo[0];
            attributeInfo = newattributeInfo;
        }
        switch (attributeInfo[0].toUpperCase()) {
            case "#N#":
                attr = (T) new NumericAttr(attributeInfo[1]);
                break;
            case "#L#":
                attr = (T) new LinguisticAttr(attributeInfo[1]);
                break;
            case "#F#":
                attr = (T) new FuzzyAttr(attributeInfo[1]);
                break;
            default:
                attr = (T) new NumericAttr(attributeInfo[1]);
        }
        for (int i = 2; i < attributeInfo.length; i++) {
            attr.addValue(attributeInfo[i]);
        }
        return attr;
    }

    private void close(BufferedReader br) {
        try {
            br.close();
        } catch (IOException ex) {
            Logger.getLogger(MyLoader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void addInstance(String[] row) {
        int attrIndex = 0;
        for (int i = 0; i < row.length;) {
            Attribute attr = getDataset().getAttribute(attrIndex++);
            switch (attr.getType()) {
                case Attribute.NUMERIC:
                    ((NumericAttr) attr).addValue(row[i++]);
                    break;
                case Attribute.LINGVISTIC:
                    ((LinguisticAttr) attr).addValue(row[i++]);
                    break;
                case Attribute.FUZZY:
                    FuzzyAttr a = ((FuzzyAttr) attr);
                    int count = a.getDomainSize();
                    for (int j = 0; j < count; j++) {
                        a.getAttrValue(j).addVaule(Double.parseDouble(row[i++]));
                    }
            }
        }
    }

}
