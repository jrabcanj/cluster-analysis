/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minig.data.io.load;

import minig.data.dataset.DataSet;

/**
 *
 * @author Jaroslav
 */
public abstract class DataLoader {

    private String path;
    private DataSet dataset;
    private String separator;

    public DataLoader(DataSet dataset, String path, String separator) {
        this.path = path;
        this.dataset = dataset;
        this.separator = separator;
    }

    public DataLoader(String path, String separator) {
        this.path = path;
        this.separator = separator;
    }

    public DataLoader(String path) {
        this.path = path;
        this.separator = ",";
        dataset = new DataSet();
    }

    public DataLoader(DataSet dataset, String path) {
        this.path = path;
        this.dataset = dataset;
    }

    public final void setDataset(DataSet dataset) {
        this.dataset = dataset;
    }

    public String getSeparator() {
        return separator;
    }

    public void setSeparator(String separator) {
        this.separator = separator;
    }

    public String getPath() {
        return path;
    }

    public DataSet getDataset() {
        return dataset;
    }

    public abstract void load();
}
