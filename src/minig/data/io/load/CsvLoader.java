/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minig.data.io.load;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.logging.Level;
import java.util.logging.Logger;
import minig.data.attribute.Attribute;
import minig.data.attribute.LinguisticAttr;
import minig.data.dataset.DataSet;

/**
 *
 * @author Jaroslav
 */
public class CsvLoader extends DataLoader {

    private boolean header = false;
    private File file;
    private BufferedReader br;

    public CsvLoader(String path) {
        super(path);
        initBufferedReader();
    }

    public CsvLoader(DataSet dataset, String path) {
        super(dataset, path);
        initBufferedReader();
    }

    public void setHeader(boolean header) {
        this.header = header;
    }

    public CsvLoader(String path, String separator) {
        super(path, separator);
        initBufferedReader();
    }

    public CsvLoader(DataSet dataset, String path, String separator) {
        super(dataset, path, separator);
        initBufferedReader();
    }

    private void initBufferedReader() {
        file = new File(getPath());
        try {
            this.br = Files.newBufferedReader(file.toPath());
        } catch (IOException ex) {
            Logger.getLogger(CsvLoader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private String[] readRow() {
        String nextline = "";
        try {
            nextline = br.readLine();
        } catch (IOException ex) {
            Logger.getLogger(CsvLoader.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (nextline == null) {
            return null;
        }
        return nextline.split(getSeparator());
    }

    private void createAttributes() {
        if (header) {
            for (String attrName : readRow()) {
                LinguisticAttr attr = new LinguisticAttr(attrName);
                getDataset().addAttribute(attr);
            }
        } else {
            String[] line = readRow();
            for (int i = 0; i < line.length; i++) {
                LinguisticAttr attr = new LinguisticAttr("Attr " + (i + 1));
                getDataset().addAttribute(attr);
            }
            try {
                br.close();
            } catch (IOException ex) {
                Logger.getLogger(CsvLoader.class.getName()).log(Level.SEVERE, null, ex);
            }
            initBufferedReader();
        }
    }

    @Override
    public void load() {
        createAttributes();
        String[] row;
        while ((row = readRow()) != null) {
            for (int i = 0; i < getDataset().getAtributteCount(); i++) {
                Attribute a = getDataset().getAttribute(i);
                a.addRow(row[i]);
            }
        }
    }

}
