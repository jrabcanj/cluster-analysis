/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minig.data.dataset;

import minig.data.dataset.instances.NewInstance;
import minig.data.attribute.Attribute;
import minig.data.attribute.FuzzyAttr;
import minig.data.attribute.LinguisticAttr;
import minig.data.attribute.NumericAttr;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;
import minig.data.attribute.AttrValue;
import application.ConsolePrintable;
import application.ProjectUtils;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.SplittableRandom;
import java.util.function.Predicate;
import minig.data.dataset.instances.DatasetInstance;
import minig.data.dataset.instances.Instance;
import minig.data.io.load.DataLoader;

/**
 *
 * @author Jaroslav
 */
public class DataSet implements ConsolePrintable {

    protected ArrayList<Attribute> attributes = new ArrayList<>(15);
    protected Attribute outputAttribute;
    private int outputAttrIndex = -1;
    private String name = "";
    private int rowLength = 0;
    private DataInfo datainfo = new DataInfo(this);

    public DataSet() {
    }

    public DataSet(DataLoader loader) {
        loader.setDataset(this);
        loader.load();
    }

    public DataSet(Attribute... attributes) {
        for (Attribute attribute : attributes) {
            addAttribute(attribute);
        }
    }

    public DataSet(String name, Attribute... attributes) {
        this.name = name;
        for (Attribute attribute : attributes) {
            addAttribute(attribute);
        }
    }

    public void addAttributes(Attribute... attrs) {
        for (Attribute attr : attrs) {
            addAttribute(attr);
        }
    }

    public <T extends Attribute> List<T> getAttributesIf(Predicate<Attribute> test) {
        ArrayList<T> list = new ArrayList(attributes.size());
        Attribute attribute;
        for (int i = 0; i < attributes.size(); i++) {
            attribute = attributes.get(i);
            if (test.test(attribute)) {
                list.add((T) attribute);
            }
        }
        return list;
    }

    public boolean isEmpty() {
        return getDataCount() == 0;
    }

    public void removeInstanceIf(Predicate<Instance> test) {
        int index = 0;
        while (getDataCount() > index) {
            DatasetInstance dataInstance = new DatasetInstance(this, index);
            if (test.test(dataInstance)) {
                removeInstance(index);
            } else {
                index++;
            }
        }
    }

    public void removeInstance(int index) {
        for (Attribute attribute : attributes) {
            attribute.removeRow(index);
        }
    }

    public int getLastAttrIndex() {
        return attributes.size() - 1;
    }

    public int getNumberOfClasses() {
        if (!isOutputAttributeSet() || Attribute.isNumeric(outputAttribute)) {
            return 0;
        }
        return this.getOutputAttr().getDomain().size();
    }

    public void shuffleData() {
        SplittableRandom sp;
        long seed = 1245888;
        for (int i = 0; i < getAtributteCount(); i++) {
            Attribute a = getAttribute(i);
            if (Attribute.isFuzzy(a)) {
                FuzzyAttr attr = (FuzzyAttr) a;
                for (AttrValue attrValue : attr.getDomain()) {
                    sp = new SplittableRandom(seed);
                    ProjectUtils.shuffle(attrValue.getValues(), sp);
                }
            } else {
                sp = new SplittableRandom(seed);
                ProjectUtils.shuffle((List) a.getValues(), sp);
            }
        }
    }

    public void shuffleAttrs() {
        SplittableRandom random = new SplittableRandom();
        for (int i = 0; i < attributes.size(); i++) {
            int randomValue = i + random.nextInt(attributes.size() - i);
            swapAttributes(i, randomValue);
        }
    }

    public void removeAttribue(int index) {
        Attribute a = attributes.get(index);
        if (a.isOutputAttr()) {
            outputAttrIndex = -1;
            outputAttribute = null;
        } else if (index < outputAttrIndex) {
            --outputAttrIndex;
        }
        rowLength -= a.getRowLength();
        attributes.remove(index);
        datainfo.removeAttribute(a);
        for (int i = index; i < getAtributteCount(); i++) {
            getAttribute(i).setAttributeIndex(i);
        }
    }

    public void removeAttribue(Attribute attr) {
        removeAttribue(attr.getAttributeIndex());
    }

    public void removeAttribueIf(Predicate<Attribute> test) {
        LinkedList<Integer> indices = new LinkedList();
        for (Attribute attribute : attributes) {
            if (test.test(attribute)) {
                indices.add(attribute.getAttributeIndex());
            }
        }
        Iterator<Integer> it = indices.descendingIterator();
        while (it.hasNext()) {
            removeAttribue(it.next());
            it.remove();
        }
    }

    /**
     * NO CASE SENSITIVE
     *
     * @param name
     * @return
     */
    public boolean removeAttribue(String name) {
        boolean deleted = false;
        for (int index = 0; index < attributes.size(); index++) {
            Attribute attribute = attributes.get(index);
            if (attribute.getName().equalsIgnoreCase(name)) {
                deleted = true;
                this.removeAttribue(index);
            }
        }
        return deleted;
    }

    public int getInputAttrCount() {
        if (isOutputAttributeSet()) {
            return attributes.size() - 1;
        } else {
            return attributes.size();
        }
    }

    public final void addAttributes(Collection<Attribute> attrs) {
        attrs.forEach((attr) -> {
            addAttribute(attr);
            if (attr.isOutputAttr()) {
                setOutputAttr();
            }
        });
    }

    public final void addAttributes(Collection<Attribute> attrs, boolean mapAttrInfo) {
        if (!mapAttrInfo) {
            addAttributes(attrs);
        } else {
            attrs.forEach((attr) -> {
                addAttribute(attr);
                if (attr.isOutputAttr()) {
                    setOutputAttr();
                }
            });
        }
    }

    public final void putAttribute(int index, Attribute attr) {
        replaceAttribute(attr, index);
    }

    public final void addAttribute(Attribute attr) {
        Attribute a = attr.getRawCopy();
        a.setIsOutputAttr(false);
        a.setAttributeIndex(attributes.size());
        a.setDataset(this);
        attributes.add(a);
        rowLength += attr.getRowLength();
        datainfo.addAttribute(a);
    }

    /**
     * Each linguistic and numeric attribute has one columns in dataset. In case
     * of fuzzy attribute, the number of its columns corresponds with number its
     * values
     *
     * @return number of columns
     */
    public int getRowLength() {
        return rowLength;
    }

    public int getOutputAttrIndex() {
        return outputAttrIndex;
    }

    public Attribute getAttribute(String name) {
        for (int index = 0; index < attributes.size(); index++) {
            Attribute attribute = attributes.get(index);
            if (attribute.getName().equalsIgnoreCase(name)) {
                return getAttribute(index);
            }
        }
        return null;
    }

    public <T extends Attribute> T getAttribute(int index) {
        return (T) attributes.get(index);
    }

    /**
     * Method create new ArrayList and all attributes are inserted into them.
     *
     * @param <A>
     * @return all attributes of dataset wrapped into ArrayList
     */
    public <A extends Attribute> List<A> getAttributesInList() {
        List<A> a = new ArrayList<>(attributes.size());
        for (int i = 0; i < attributes.size(); i++) {
            A attr = this.<A>getAttribute(i);
            a.add(attr);
        }
        return a;
    }

    public List<Attribute> getAttributes() {
        return attributes;
    }

    public <A extends Attribute> List<A> getAttributes(int... indexes) {
        List<A> a = new ArrayList<>(indexes.length);
        for (int i = 0; i < indexes.length; i++) {
            int ai = indexes[i];
            a.add(getAttribute(ai));
        }
        return a;
    }

    /**
     *
     * @param attribute
     * @return Index of first attribute with the same name as param attr. If
     * attribute is not present in list, then -1 is returned.
     */
    public int getAttributeIndex(Attribute attribute) {
        return attribute.getAttributeIndex();
    }

    public int getDataCount() {
        if (attributes.isEmpty()) {
            return 0;
        }
        return attributes.get(0).getDataCount();
    }

    //TODO test
    public int getAttributeIndex(String attrName) {
        int i = 0;
        for (; i < attributes.size(); i++) {
            Attribute get = attributes.get(i);
            if (get.getName().equals(attrName)) {
                break;
            }
        }
        return i;
    }

    public int getAtributteCount() {
        return attributes.size();
    }

    public void replaceAttribute(Attribute added, int indexOfReplaced) {
        replaceAttribute(added, indexOfReplaced, true);
    }

    public void replaceAttribute(Attribute added, int indexOfReplaced, boolean makeCopy) {
        Attribute inserted = makeCopy ? added.getRawCopy() : added;
        rowLength -= attributes.get(indexOfReplaced).getRowLength();
        inserted.setAttributeIndex(indexOfReplaced);
        inserted.setDataset(this);
        datainfo.removeAttribute(getAttribute(indexOfReplaced));
        attributes.set(indexOfReplaced, inserted);
        rowLength += added.getRowLength();
        if (getOutputAttrIndex() == indexOfReplaced) {
            outputAttrIndex = -1;
            outputAttribute = null;
            inserted.setIsOutputAttr(false);
        }
        datainfo.addAttribute(inserted);
    }

    public void replaceAttribute(Attribute inserted, Attribute replaced) {
        int index = replaced.getAttributeIndex();
        replaceAttribute(inserted, index);
    }

    public final void setOutputAttrIndex(int outputAttIndex) {
        outputAttribute = attributes.get(outputAttIndex);
        outputAttribute.setIsOutputAttr(true);
        this.outputAttrIndex = outputAttIndex;
    }

    /**
     * The last attribute of the dataset is set as the output attribute.
     */
    public final void setOutputAttr() {
        outputAttribute = attributes.get(getLastAttrIndex());
        outputAttribute.setIsOutputAttr(true);
        this.outputAttrIndex = getLastAttrIndex();
    }

    public List<Double> getRowVector(int index) {
        List<Double> attrs = new ArrayList(getAtributteCount());
        for (int i = 0; i < getAtributteCount(); i++) {
            Attribute attr = getAttribute(i);
            if (Attribute.isNumeric(attr)) {
                attrs.add(((NumericAttr) attr).getAttrValue().get(index));
            } else if (Attribute.isFuzzy(attr)) {
                for (AttrValue attrValue : ((FuzzyAttr) attr).getDomain()) {
                    attrs.add(attrValue.get(index));
                }
            }
        }
        return attrs;
    }

    public void swapAttributes(final int indexAttr1, final int indexAttr2) {
        final Attribute a1 = attributes.get(indexAttr1);
        final Attribute a2 = attributes.get(indexAttr2);
        a1.setAttributeIndex(indexAttr2);
        a2.setAttributeIndex(indexAttr1);
        if (a1.isOutputAttr()) {
            outputAttrIndex = a1.getAttributeIndex();
        } else if (a2.isOutputAttr()) {
            outputAttrIndex = a2.getAttributeIndex();
        }
        attributes.set(indexAttr1, a2);
        attributes.set(indexAttr2, a1);
    }

    /**
     * Metoda zakazdym vytvori novy list a okdazy na atributy tam presuva v
     * cykle. Zlozitost n
     *
     * @param <T>
     * @return
     */
    public <T extends Attribute> List<T> getInputAttrs() {
//        if (outputAttrIndex == attributes.size() - 1) {
//            return (List<T>) attributes.subList(0, attributes.size() - 1);
//        }
        List<T> attrs = new ArrayList<>(attributes.size());
        for (int i = 0; i < attributes.size(); i++) {
            T attr = (T) attributes.get(i);
            if (!attr.isOutputAttr()) {
                attrs.add(attr);
            }
        }
        return (List<T>) attrs;
    }

    public <A extends Attribute> A getOutputAttr() {
        if (outputAttribute == null) {
            throw new Error("The output attribute is not set");
        }
        return (A) outputAttribute;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(10000);
        for (Attribute attribute : attributes) {
            sb.append(String.format("%-16s", attribute.getName().toUpperCase()));
        }
        sb.append(System.lineSeparator());
        for (int i = 0; i < getDataCount(); i++) {
            for (Attribute attribute : attributes) {
                sb.append(String.format("%-16s", attribute.getRowString(i))).append("|");
            }
            sb.append(System.lineSeparator());
        }
        return sb.toString();
    }

    /**
     * Method prints specified number of lines to the console.
     * @param lineCount number of printed lines.
     */
    public void print(int lineCount) {
        StringBuilder sb = new StringBuilder();
        for (Attribute attribute : attributes) {
            sb.append(String.format("%-16s", attribute.getName().toUpperCase()));
        }
        sb.append(System.lineSeparator());
        for (int i = 0; i < lineCount; i++) {
            for (Attribute attribute : attributes) {
                sb.append(String.format("%-16s", attribute.getRowString(i))).append("|");
            }
            sb.append(System.lineSeparator());
        }
        System.out.println(sb.toString());
    }

    public String dataToString(String separator) {
        StringBuilder sb = new StringBuilder();
        sb.append(System.lineSeparator());
        for (int i = 0; i < getDataCount(); i++) {
            for (int j = 0; j < getAtributteCount(); j++) {
                Attribute attribute = getAttribute(j);
                sb.append(attribute.getUnformatedRowString(i));
                if (j < (getAtributteCount() - 1)) {
                    sb.append(separator);
                }
            }
            sb.append(System.lineSeparator());
        }
        return sb.toString();
    }

    public void printData() {
        ConsolePrintable.println(dataToString(","));
    }

    public void forEachAttr(Consumer<Attribute> consumer) {
        attributes.forEach(consumer);
    }

    public void forEachAttrIf(Predicate<Attribute> test, Consumer<Attribute> consumer) {
        for (Attribute attribute : attributes) {
            if (test.test(attribute)) {
                consumer.accept(attribute);
            }
        }
    }

    public void forEachInputAttr(Consumer<Attribute> consumer) {
        for (Attribute attribute : attributes) {
            if (!attribute.isOutputAttr()) {
                consumer.accept(attribute);
            }
        }
    }

    public void forEachNumericAttr(Consumer<NumericAttr> consumer) {
        for (Attribute attribute : attributes) {
            if (attribute.isNumeric()) {
                consumer.accept(attribute.numeric());
            }
        }
    }

    public void forEachFuzzyAttr(Consumer<FuzzyAttr> consumer) {
        for (Attribute attribute : attributes) {
            if (attribute.isFuzzy()) {
                consumer.accept(attribute.fuzzy());
            }
        }
    }

    public void forEachLingvisticAttr(Consumer<LinguisticAttr> consumer) {
        for (Attribute attribute : attributes) {
            if (attribute.isLingvistic()) {
                consumer.accept(attribute.lingvistic());
            }
        }
    }

    public void rearangeOrder() {
        if (outputAttrIndex != getLastAttrIndex()) {
            this.swapAttributes(outputAttrIndex, getLastAttrIndex());
        }
    }

    /**
     * dataset, obsahuje attr, ale bez hodnot
     *
     * @return
     */
    public DataSet getEmptyCopy() {
        DataSet fd = new DataSet();
        for (Attribute attribute : attributes) {
            Attribute a = attribute.getEmptyCopy();
            fd.addAttribute(a);
        }
        fd.setOutputAttrIndex(getOutputAttrIndex());
        return fd;
    }

    public List<Object> getRow(int index) {
        List<Object> values = new ArrayList<>(attributes.size());
        for (int i = 0; i < attributes.size(); i++) {
            Attribute attr = attributes.get(i);
            if (attr.getDataCount() > index) {
                values.add(attr.getRow(index));
            }
        }
        return values;
    }

    public NewInstance getInstance(int index) {
        NewInstance i = new NewInstance(this, getRow(index));
        return i;
    }

    public DatasetInstance getLinkedInstance(int index) {
        DatasetInstance i = new DatasetInstance(this, index);
        return i;
    }

    public List<Instance> getInstances() {
        ArrayList<Instance> instances = new ArrayList<>(getDataCount());
        for (int i = 0; i < getDataCount(); i++) {
            instances.add(getLinkedInstance(i));
        }
        return instances;
    }

    public List<DatasetInstance> getDatasetInstances() {
        ArrayList<DatasetInstance> instances = new ArrayList<>(getDataCount());
        for (int i = 0; i < getDataCount(); i++) {
            instances.add(getLinkedInstance(i));
        }
        return instances;
    }

    public void addInstance(List<Object> row) {
        int i = 0;
        for (Attribute attr : attributes) {
            switch (attr.getType()) {
                case Attribute.NUMERIC:
                    ((NumericAttr) attr).addValue((Double) row.get(i++));
                    break;
                case Attribute.LINGVISTIC:
                    ((LinguisticAttr) attr).addValue((String) row.get(i++));
                    break;
                case Attribute.FUZZY:
                    FuzzyAttr a = ((FuzzyAttr) attr);
                    a.addFuzzyRow((List<Double>) row.get(i++));
            }
        }
    }

    public void addInstance(Instance instance) {
        this.addInstance(instance.getValues());
    }

    public double[][] getDataInDoubleArray(boolean withOutputAttr) {
        double[][] data;
        int attrc;
        if (!withOutputAttr) {
            if (isOutputAttributeSet()) {
                attrc = getAtributteCount() - 1;
            } else {
                attrc = getAtributteCount();
            }
        } else {
            if (isOutputAttributeSet()) {
                attrc = getAtributteCount();
            } else {
                attrc = getAtributteCount();
            }
        }

        data = new double[getDataCount()][attrc];
        for (int i = 0; i < getDataCount(); i++) { // riadok
            int pom = 0;
            for (int j = 0; j < attrc + pom; j++) { //stlpce
                if (isOutputAttributeSet() && withOutputAttr != true && getOutputAttrIndex() == j) {
                    pom = 1;
                    continue;
                }
                NumericAttr num = getAttribute(j);
                double value = num.getValues().get(i);
                data[i][j - pom] = value;
            }
        }
        return data;
    }

    public double[] getDataInDoubleArray(boolean withOutputAttr, int row) {
        double[] data;
        int attrc;
        if (!withOutputAttr) {
            if (isOutputAttributeSet()) {
                attrc = getAtributteCount() - 1;
            } else {
                attrc = getAtributteCount();
            }
        } else {
            if (isOutputAttributeSet()) {
                attrc = getAtributteCount();
            } else {
                attrc = getAtributteCount();
            }
        }

        data = new double[attrc];
        int pom = 0;
        for (int j = 0; j < attrc + pom; j++) { //stlpce
            if (isOutputAttributeSet() && withOutputAttr != true && getOutputAttrIndex() == j) {
                pom = 1;
                continue;
            }
            NumericAttr num = getAttribute(j);
            double value = num.getValues().get(row);
            data[j - pom] = value;
        }
        return data;
    }

    /**
     * ONLY NUMERIC ATTRIBUTES.
     *
     * @param row
     * @param data
     * @return
     */
    public void getDataIntoDoubleArray(int row, double[] data) {
        NumericAttr num;
        for (int j = 0; j < getAtributteCount(); j++) { //stlpce
            if (Attribute.isNumeric(attributes.get(j))) {
                num = attributes.get(j).numeric();
                double value = num.get(row);
                data[j] = value;
            }
        }
    }

    public void addInstance(Object... values) {
        for (int j = 0; j < attributes.size(); j++) {
            Attribute attr = attributes.get(j);
            switch (attr.getType()) {
                case Attribute.NUMERIC:
                    ((NumericAttr) attr).addValue((Double) values[j]);
                    break;
                case Attribute.LINGVISTIC:
                    ((LinguisticAttr) attr).addValue((String) values[j]);
                    break;
                case Attribute.FUZZY:
                    FuzzyAttr a = ((FuzzyAttr) attr);
                    a.addFuzzyRow((List<Double>) values[j]);
                    break;
            }
        }
    }

    /**
     *
     * @return atributy neobsahuju hodnoty instancii
     */
    public List<Attribute> getEmptyAttributes() {
        List<Attribute> attrs = new ArrayList<>();
        attributes.stream().forEach((attr) -> {
            attrs.add(attr.getEmptyCopy());
        });
        return attrs;
    }

    public DataInfo getDatainfo() {
        return datainfo;
    }

    public boolean isOutputAttributeSet() {
        return outputAttrIndex != -1;
    }

    public boolean hasOutputAttr() {
        return outputAttrIndex != -1;
    }

    public static void main(String[] args) throws IOException {
//        final DataSet dataset = DatasetFactory.getDataset(DatasetFactory.GINI_EXAMPLE);
//        System.out.println(dataset.getDataCount());
//        dataset.removeInstanceIf((t) -> t.getClassIndex(dataset.getAttribute(1)) == 1);
//        dataset.print();
//        Distance d = new Euclidian();
        //dataset.getInstance(2).print();
        //dataset.getLinkedInstance(2).print();
    }

}
