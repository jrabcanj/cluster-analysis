/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minig.data.dataset;

import minig.data.attribute.Attribute;
import minig.data.attribute.FuzzyAttr;
import minig.data.attribute.LinguisticAttr;

/**
 *
 * @author Jaroslav
 */
public class DataInfo {

    private DataSet dataset;

    private short numAttrCount;
    private short fuzzyAttrCount;
    private short lngAttrCount;

    DataInfo() {
    }

    DataInfo(DataSet dataset) {
        this.dataset = dataset;
    }

    public boolean isOutputAttrLast() {
        int lastIndex = dataset.getLastAttrIndex();
        return dataset.getOutputAttr().getAttributeIndex() == lastIndex;
    }

    public boolean isBinaryClass() {
        if (!dataset.isOutputAttributeSet()) {
            return false;
        }
        if (Attribute.isFuzzy(dataset.getOutputAttr())) {
            FuzzyAttr fa = dataset.getOutputAttr();
            return fa.getDomainSize() == 2;
        } else if (Attribute.isLingvistic(dataset.getOutputAttr())) {
            LinguisticAttr la = dataset.getOutputAttr();
            return la.getDomainSize() == 2;
        }
        return false;
    }

    public short getNumAttrCount() {
        return numAttrCount;
    }

    public void setNumAttrCount(short numAttrCount) {
        this.numAttrCount = numAttrCount;
    }

    public short getFuzzyAttrCount() {
        return fuzzyAttrCount;
    }

    public void setFuzzyAttrCount(short fuzzyAttrCount) {
        this.fuzzyAttrCount = fuzzyAttrCount;
    }

    public short getLngAttrCount() {
        return lngAttrCount;
    }

    public void setLngAttrCount(short lngAttrCount) {
        this.lngAttrCount = lngAttrCount;
    }

    public void addAttribute(Attribute a) {
        if (Attribute.isFuzzy(a)) {
            fuzzyAttrCount++;
        } else if (Attribute.isLingvistic(a)) {
            lngAttrCount++;
        } else {
            numAttrCount++;
        }
    }

    public void removeAttribute(Attribute a) {
        if (Attribute.isFuzzy(a)) {
            fuzzyAttrCount--;
        } else if (Attribute.isLingvistic(a)) {
            lngAttrCount--;
        } else {
            numAttrCount--;
        }
    }

    public boolean areNumericData(boolean includeOA) {
        short target = numAttrCount;
        if (includeOA) {
            if (Attribute.isNumeric(dataset.getOutputAttr())) {
                target--;
            } else {
                return false;
            }
        }
        boolean r = target > 0 && fuzzyAttrCount == 0 && lngAttrCount == 0;
        return r;
    }

    public boolean areFuzzyData(boolean includeOA) {
        short target = fuzzyAttrCount;
        if (includeOA) {
            if (Attribute.isFuzzy(dataset.getOutputAttr())) {
                target--;
            } else {
                return false;
            }
        }
        boolean r = target > 0 && numAttrCount == 0 && lngAttrCount == 0;
        return r;
    }

}
