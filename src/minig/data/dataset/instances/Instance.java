/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minig.data.dataset.instances;

import application.ConsolePrintable;
import application.stat.Max;
import application.structures.BoxedDoubleArray;
import java.util.LinkedList;
import java.util.List;
import minig.data.attribute.AttrValue;
import minig.data.attribute.Attribute;
import minig.data.attribute.CategoricalAttr;
import minig.data.attribute.FuzzyAttr;
import minig.data.attribute.LinguisticAttr;
import minig.data.attribute.NumericAttr;
import minig.data.dataset.DataSet;

/**
 *
 * @author Jaroslav
 */
public abstract class Instance implements ConsolePrintable {

    /**
     * If output attribute is set, then the output value is returned, else null.
     *
     * @param <T> in case of numeric attribute, doulbe is returned.
     * @return depends on the type of attribute. <BR>
     * -Fuzzy attr = doublevector -Num attr = double -ligvs attr = ? [need check
     * :P]
     */
    public abstract <T> T getOutputValue();


    public abstract List<Object> getValues();


    public abstract Object getValueOfAttribute(Attribute attr);


    public abstract int getClassIndex(CategoricalAttr attr);

    public abstract Object getValueOfAttribute(int attrIndex);

    public abstract double getValueForAttrVal(AttrValue val);

    public abstract void setDataset(DataSet dataset);

 
    public abstract <T> T getValue(int index);

    public abstract double getValue(NumericAttr attr);

    public abstract List<Double> getValue(FuzzyAttr attr);

    public abstract String getValue(LinguisticAttr attr);

    public abstract List getFeatureWithoutOutAttr();

    public abstract double[] getInputNumericData();

    public abstract double[] getNumericData();

    public abstract List<Object> getFeatureVector();

    public abstract List<Double> getAttrValueVector();

    public abstract List<Double> getAttrValueVectorWithoutOutputAttr();

    @Override
    public abstract String toString();

    public abstract String toInstanceString();

    public static List<Instance> where(List<Instance> instances, CategoricalAttr attr, int classIndex) {
        List<Instance> inst = new LinkedList<>();
        for (Instance instance : instances) {
            if (instance.getClassIndex(attr) == classIndex) {
                inst.add(instance);
            }
        }
        return inst;
    }

    public static List<Instance> whereLessThan(List<Instance> instances, NumericAttr attr, double border) {
        List<Instance> inst = new LinkedList<>();
        for (Instance instance : instances) {
            if (instance.getValue(attr) < border) {
                inst.add(instance);
            }
        }
        return inst;
    }

    public static List<Instance>[] split(List<Instance> instances, NumericAttr attr, double splitPoint) {
        LinkedList<Instance> inst = new LinkedList<>();
        LinkedList<Instance> inst2 = new LinkedList<>();
        for (Instance instance : instances) {
            if (instance.getValue(attr) < splitPoint) {
                inst.add(instance);
            } else {
                inst2.add(instance);
            }
        }
        LinkedList<Instance>[] s = new LinkedList[2];
        s[0] = inst;
        s[1] = inst2;
        return s;
    }

    /**
     * Labels are computed by class indices. Not suitable for Fuzzy Attributes
     *
     * @param instances
     * @param output
     * @return
     */
    public static List<Double> computeLabels(List<Instance> instances, CategoricalAttr output) {
        double[] arr = new double[output.getDomainSize()];
        if (instances.isEmpty()) {
            for (int i = 0; i < arr.length; i++) {
                arr[i] = 1d / output.getDomainSize();
            }
        } else {
            for (Instance instance : instances) {
                arr[instance.getClassIndex(output)]++;
            }
            for (int i = 0; i < arr.length; i++) {
                arr[i] /= instances.size();
            }
        }
        return new BoxedDoubleArray(arr);
    }

    /**
     *
     * @param instances
     * @param output
     * @param inputParamMax - Input parameter. The maximal value of confidence
     * level is saved into this parameter.
     * @return
     */
    public static List<Double> computeLabels(List<Instance> instances, CategoricalAttr output, Max inputParamMax) {
        double[] arr = new double[output.getDomainSize()];
        if (instances.isEmpty()) {
            for (int i = 0; i < arr.length; i++) {
                arr[i] = 1d / output.getDomainSize();
            }
        } else {
            for (Instance instance : instances) {
                arr[instance.getClassIndex(output)]++;
            }
            for (int i = 0; i < arr.length; i++) {
                arr[i] /= instances.size();
                inputParamMax.add(arr[i]);
            }
        }
        return new BoxedDoubleArray(arr);
    }

    public static double[] toArray(List<Instance> instances, NumericAttr attr) {
        double[] arr = new double[instances.size()];
        int i = 0;
        for (Instance instance : instances) {
            arr[i++] = instance.getValue(attr);
        }
        return arr;
    }

}
