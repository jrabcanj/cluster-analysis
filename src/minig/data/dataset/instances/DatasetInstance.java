/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minig.data.dataset.instances;

import application.ConsolePrintable;
import java.util.ArrayList;
import java.util.List;
import minig.data.attribute.AttrValue;
import minig.data.attribute.Attribute;
import minig.data.attribute.CategoricalAttr;
import minig.data.attribute.FuzzyAttr;
import minig.data.attribute.LinguisticAttr;
import minig.data.attribute.NumericAttr;
import minig.data.dataset.DataSet;

/**
 *
 * @author Jaroslav
 */
public class DatasetInstance extends Instance implements ConsolePrintable {

    private DataSet dataset;
    private int index;

    public DatasetInstance(DataSet dt, int rowIndex) {
        this.dataset = dt;
        this.index = rowIndex;
    }

    public <T> T getOutputValue() {
        return (T) dataset.getOutputAttr().getRow(index);
    }

    public List<Object> getValues() {
        return dataset.getRow(index);
    }

    public void setDataset(DataSet dataset) {
        this.dataset = dataset;
    }

    public <T> T getValue(int index) {
        return (T) dataset.getAttribute(index).getRow(this.index);
    }

    public int getIndex() {
        return index;
    }

    public List getFeatureWithoutOutAttr() {
        List attrs = new ArrayList(dataset.getAtributteCount());
        for (int i = 0; i < dataset.getAtributteCount(); i++) {
            Attribute attr = dataset.getAttribute(i);
            if (!attr.isOutputAttr()) {
                attrs.add(getValue(i));
            }
        }
        return attrs;
    }

    public DataSet getDataset() {
        return dataset;
    }

    public double[] getInputNumericData() {
        double[] arr = new double[dataset.getInputAttrCount()];
        for (int i = 0; i < dataset.getInputAttrCount(); i++) {
            Attribute attr = dataset.getAttribute(i);
            if (attr.isInputAttr() && attr.isNumeric()) {
                arr[i] = getValue(i);
            }
        }
        return arr;
    }

    public double[] getNumericData() {
        double[] arr = new double[dataset.getInputAttrCount()];
        for (int i = 0; i < dataset.getInputAttrCount(); i++) {
            Attribute attr = dataset.getAttribute(i);
            if (attr.isNumeric()) {
                arr[i] = getValue(i);
            }
        }
        return arr;
    }

    public List<Object> getFeatureVector() {
        return getValues();
    }

    public List<Double> getAttrValueVector() {
        List<Double> attrs = new ArrayList(dataset.getAtributteCount());
        for (int i = 0; i < dataset.getAtributteCount(); i++) {
            Attribute attr = dataset.getAttribute(i);
            if (Attribute.isNumeric(attr)) {
                attrs.add((Double) getValue(index));
            } else if (Attribute.isFuzzy(attr)) {
                for (AttrValue attrValue : ((FuzzyAttr) attr).getDomain()) {
                    attrs.add(attrValue.get(i));
                }
            }
        }
        return attrs;
    }

    public List<Double> getAttrValueVectorWithoutOutputAttr() {
        List<Double> attrs = new ArrayList(dataset.getInputAttrCount());
        for (int i = 0; i < dataset.getAtributteCount(); i++) {
            Attribute attr = dataset.getAttribute(i);
            if (attr.isOutputAttr()) {
                continue;
            }
            if (Attribute.isNumeric(attr)) {
                NumericAttr nattr = (NumericAttr) attr;
                attrs.add(getValueForAttrVal(nattr.getAttrValue()));
            } else if (Attribute.isFuzzy(attr)) {
                for (AttrValue attrValue : ((FuzzyAttr) attr).getDomain()) {
                    attrs.add(getValueForAttrVal(attrValue));
                }
            }
        }
        return attrs;
    }

    /**
     * Attribute must by from dataset
     *
     * @param attr
     * @return
     */
    public Object getValueOfAttribute(Attribute attr) {
        return dataset.getAttribute(attr.getAttributeIndex()).getRow(index);
    }

    public Object getValueOfAttribute(int attrIndex) {
        return getValue(attrIndex);
    }

    public double getValueForAttrVal(AttrValue val) {
        Attribute attr = val.getAttribute();
        if (Attribute.isFuzzy(attr)) {
            List<Double> attrVals = (List<Double>) getValueOfAttribute(attr);
            return (double) attrVals.get(val.getIndexOfValue());
        } else {
            return (double) getValueOfAttribute(attr.getAttributeIndex());
        }
    }

    @Override
    public String toString() {
        return getFeatureVector().toString();
    }

    public String toInstanceString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < dataset.getAtributteCount(); i++) {
            Object get = getValue(i);
            Attribute attr = (Attribute) dataset.getAttribute(i);
            sb.append(attr.getAttributeIndex()).append(attr.getName()).append(get).append(System.getProperty("line.separator"));
        }
        return sb.toString();
    }

    @Override
    //TODO FIX
    public int getClassIndex(CategoricalAttr attr) {
        if (attr.getType() > 0) {
            return dataset.<CategoricalAttr>getAttribute(attr.getAttributeIndex()).getClassIndex(index);
        } else {
            return attr.getClassIndex(index);
        }
    }

    public int getClassIndex(LinguisticAttr attr) {
        return dataset.<LinguisticAttr>getAttribute(attr.getAttributeIndex()).getClassIndex(index);
    }

    @Override
    public double getValue(NumericAttr attr) {
        return dataset.getAttribute(attr.getAttributeIndex()).numeric().get(index);
    }

    @Override
    public List<Double> getValue(FuzzyAttr attr) {
        return dataset.getAttribute(attr.getAttributeIndex()).fuzzy().getRow(index);
    }

    public FuzzyAttr.BoxedRow getValueInBoxedRow(FuzzyAttr attr) {
        return dataset.getAttribute(attr.getAttributeIndex()).fuzzy().getBoxedRow(index);
    }

    @Override
    public String getValue(LinguisticAttr attr) {
        return (String) dataset.getAttribute(attr.getAttributeIndex()).getRow(index);
    }

}
