/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minig.data.dataset.instances;

import application.ConsolePrintable;
import application.ProjectUtils;
import java.util.ArrayList;
import java.util.List;
import minig.data.attribute.AttrValue;
import minig.data.attribute.Attribute;
import minig.data.attribute.CategoricalAttr;
import minig.data.attribute.FuzzyAttr;
import minig.data.attribute.LinguisticAttr;
import minig.data.attribute.NumericAttr;
import minig.data.dataset.DataSet;

/**
 *
 * @author Jaroslav
 */
public class NewInstance extends Instance implements ConsolePrintable {

    private DataSet dataset;
    private List<Object> values;

    public NewInstance(DataSet dt, List<Object> values) {
        this.dataset = dt;
        this.values = values;
    }

    public NewInstance(List<Object> values) {
        this.values = values;
    }

    public NewInstance(DataSet dt) {
        this.dataset = dt;
    }

    public <T> T getOutputValue() {
        int index = dataset.getOutputAttrIndex();
        return (T) values.get(index);
    }

    public List<Object> getValues() {
        return values;
    }

    public void setDataset(DataSet dataset) {
        this.dataset = dataset;
    }

    public <T> T getValue(int index) {
        return (T) values.get(index);
    }

    public List getFeatureWithoutOutAttr() {
        List attrs = new ArrayList(dataset.getAtributteCount());
        for (int i = 0; i < dataset.getAtributteCount(); i++) {
            Attribute attr = dataset.getAttribute(i);
            if (!attr.isOutputAttr()) {
                attrs.add(values.get(i));
            }
        }
        return attrs;
    }

    public double[] getInputNumericData() {
        double[] arr = new double[dataset.getInputAttrCount()];
        for (int i = 0; i < dataset.getInputAttrCount(); i++) {
            Attribute attr = dataset.getAttribute(i);
            if (attr.isInputAttr() && attr.isNumeric()) {
                arr[i] = getValue(i);
            }
        }
        return arr;
    }

    public double[] getNumericData() {
        double[] arr = new double[dataset.getInputAttrCount()];
        for (int i = 0; i < dataset.getInputAttrCount(); i++) {
            Attribute attr = dataset.getAttribute(i);
            if (attr.isNumeric()) {
                arr[i] = getValue(i);
            }
        }
        return arr;
    }

    public List<Object> getFeatureVector() {
        return values;
    }

    public List<Double> getAttrValueVector() {
        List<Double> attrs = new ArrayList(dataset.getAtributteCount());
        for (int i = 0; i < dataset.getAtributteCount(); i++) {
            Attribute attr = dataset.getAttribute(i);
            if (Attribute.isNumeric(attr)) {
                attrs.add((Double) values.get(i));
            } else if (Attribute.isFuzzy(attr)) {
                for (AttrValue attrValue : ((FuzzyAttr) attr).getDomain()) {
                    attrs.add(attrValue.get(i));
                }
            }
        }
        return attrs;
    }

    public List<Double> getAttrValueVectorWithoutOutputAttr() {
        List<Double> attrs = new ArrayList(dataset.getInputAttrCount());
        for (int i = 0; i < dataset.getAtributteCount(); i++) {
            Attribute attr = dataset.getAttribute(i);
            if (attr.isOutputAttr()) {
                continue;
            }
            if (Attribute.isNumeric(attr)) {
                NumericAttr nattr = (NumericAttr) attr;
                attrs.add(getValueForAttrVal(nattr.getAttrValue()));
            } else if (Attribute.isFuzzy(attr)) {
                for (AttrValue attrValue : ((FuzzyAttr) attr).getDomain()) {
                    attrs.add(getValueForAttrVal(attrValue));
                }
            }
        }
        return attrs;
    }

    /**
     *
     * @param attr
     * @return
     */
    public Object getValueOfAttribute(Attribute attr) {
        int i = attr.getAttributeIndex();
        return values.get(i);
    }

    public Object getValueOfAttribute(int attrIndex) {
        return values.get(attrIndex);
    }

    public double getValueForAttrVal(AttrValue val) {
        Attribute attr = val.getAttribute();
        if (Attribute.isFuzzy(attr)) {
            List<Double> attrVals = (List<Double>) getValueOfAttribute(attr);
            return (double) attrVals.get(val.getIndexOfValue());
        } else {
            return (double) getValueOfAttribute(attr.getAttributeIndex());
        }
    }

    @Override
    public String toString() {
        return values.toString();
    }

    public String toInstanceString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < values.size(); i++) {
            Object get = values.get(i);
            Attribute attr = (Attribute) dataset.getAttribute(i);
            sb.append(attr.getAttributeIndex()).append(attr.getName()).append(get).append(System.getProperty("line.separator"));
        }
        return sb.toString();
    }

    @Override
    public int getClassIndex(CategoricalAttr attr) {
        int index;
        if (attr.isFuzzy()) {
            index = ProjectUtils.getMaxValueIndex(this.<List<Double>>getValue(attr.getAttributeIndex()));
            return attr.getClassIndex(index);
        } else {
            LinguisticAttr a = (LinguisticAttr) attr;
            index = a.getAttrValueIndex((String.valueOf(values.get(a.getAttributeIndex()))));
            return index;
        }
    }

    @Override
    public double getValue(NumericAttr attr) {
        return (double) values.get(attr.getAttributeIndex());
    }

    @Override
    public List<Double> getValue(FuzzyAttr attr) {
        return (List<Double>) values.get(attr.getAttributeIndex());
    }

    @Override
    public String getValue(LinguisticAttr attr) {
        return (String) values.get(attr.getAttributeIndex());
    }

}
