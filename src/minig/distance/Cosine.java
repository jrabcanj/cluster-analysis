/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minig.distance;

import minig.data.attribute.Attribute;
import minig.data.dataset.DataSet;
import minig.data.dataset.instances.Instance;

/**
 *
 * @author Jaroslav
 */
public class Cosine extends Distance {

    public Cosine(DataSet dt) {
        super(dt);
    }

    public Cosine() {
    }

    @Override
    public double getDistance(int id, double[] c) {
        double distance, a, b;
        double sumAB = 0, sumAp2 = 0, sumBp2 = 0;
        for (int i = 0; i < c.length; i++) {
            Attribute attr = getDt().getAttribute(i);
            if (Attribute.isNumeric(attr)) {
                a = c[i];
                b = attr.numeric().get(id);
                sumAB += a * b;
                sumAp2 += a * a;
                sumBp2 += b * b;
            }
        }
        distance = sumAB;
        sumAp2 = Math.sqrt(sumAp2);
        sumBp2 = Math.sqrt(sumBp2);
        if (sumAp2 == 0 || sumBp2 == 0) {
            return 0;
        }
        distance = distance / sumAp2;
        distance = distance / sumBp2;
        return 1 - distance;
    }

    @Override
    public double getDistance(int id, int id2) {
        double distance = 0;
        double sumAB = 0, sumAp2 = 0, sumBp2 = 0;
        for (int i = 0; i < getDt().getAtributteCount(); i++) {
            Attribute attr = getDt().getAttribute(i);
            if (Attribute.isNumeric(attr)) {
                double a = (double) attr.getRow(id);
                double b = (double) attr.getRow(id2);
                sumAB += a * b;
                sumAp2 += a * a;
                sumBp2 += b * b;
            }
        }
        distance = sumAB;
        sumAp2 = Math.sqrt(sumAp2);
        sumBp2 = Math.sqrt(sumBp2);
        if (sumAp2 == 0 || sumBp2 == 0) {
            return 0;
        }
        distance = distance / sumAp2;
        distance = distance / sumBp2;
        return 1 - distance;
    }

    @Override
    public double getDistance(Instance instance, double[] centroid) {
        double distance = 0;
        double sumAB = 0, sumAp2 = 0, sumBp2 = 0;
        double[] instList = instance.getNumericData();
        for (int i = 0; i < instList.length; i++) {
            double a = (double) instList[i];
            double b = centroid[i];
            sumAB += a * b;
            sumAp2 += a * a;
            sumBp2 += b * b;
        }
        distance = sumAB;
        sumAp2 = Math.sqrt(sumAp2);
        sumBp2 = Math.sqrt(sumBp2);
        if (sumAp2 == 0 || sumBp2 == 0) {
            return 0;
        }
        distance = distance / sumAp2;
        distance = distance / sumBp2;
        return 1 - distance;
    }

    @Override
    public double getDistance(double[] a, double[] b) {
        double distance;
        double sumAB = 0, sumAp2 = 0, sumBp2 = 0;
        for (int i = 0; i < a.length; i++) {
            sumAB += a[i] * b[i];
            sumAp2 += a[i] * a[i];
            sumBp2 += b[i] * b[i];
        }
        distance = sumAB;
        sumAp2 = Math.sqrt(sumAp2);
        sumBp2 = Math.sqrt(sumBp2);
        if (sumAp2 == 0 || sumBp2 == 0) {
            return 0;
        }
        distance = distance / sumAp2;
        distance = distance / sumBp2;
        return 1 - distance;
    }
}
