/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minig.distance;

import minig.data.attribute.Attribute;

/**
 *
 * @author Jaroslav
 */
public class BrayCurtis extends Distance {

    @Override
    public double getDistance(int id, int id2) {
        double c = 0, m = 0, x, y;
        for (int i = 0; i < getDt().getDatainfo().getNumAttrCount();) {
            Attribute attr = getDt().getAttribute(i);
            if (Attribute.isNumeric(attr)) {
                x = (double) attr.getRow(id);
                y = (double) attr.getRow(id2);
                c += Math.abs(y - x);
                m += Math.abs(y + x);
                i++;
            }
        }
        return c / m;
    }

    @Override
    public double getDistance(int id, double[] a) {
        double c = 0, m = 0, x;
        for (int i = 0; i < a.length; i++) {
            Attribute attr = getDt().getAttribute(i);
            if (Attribute.isNumeric(attr)) {
                x = (double) attr.getRow(id);
                c += Math.abs(a[i] - x);
                m += Math.abs(a[i] + x);
            }
        }
        return c / m;
    }

    @Override
    public double getDistance(double[] x, double[] a) {
        double c = 0, m = 0;
        for (int i = 0; i < a.length; i++) {
            c += Math.abs(a[i] - x[i]);
            m += Math.abs(a[i] + x[i]);
        }
        return c / m;
    }

}
