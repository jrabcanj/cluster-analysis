/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minig.distance;

import minig.data.dataset.DataSet;
import minig.data.dataset.instances.Instance;

/**
 *
 * @author Jaroslav
 */
public abstract class Distance {

    public enum DistanceCode {
        Euclidian, Manhatan, Cosine, BrayCurtis, Canberra, Chebyshev, Jaccard, Minkowski, PearsonCoef, 
        SorensenDice, SquaredEuclidian, WuYang;

        private String name;

        static {
            Euclidian.name = "Euclidian";
            Manhatan.name = "Manhatan distance";
            Cosine.name = "Cosine distance";
            BrayCurtis.name = "BrayCurtis distance";
            Canberra.name = "Canberra distance";
            Chebyshev.name = "Chebyshev distance";
            Jaccard.name = "Jaccard distance";
            Minkowski.name = "Minkowski distance";
            PearsonCoef.name = "PearsonCoef distance";
            SorensenDice.name = "SorensenDice distance";
            SquaredEuclidian.name = "SquaredEuclidian distance";
            WuYang.name = "WuYang distance";
        }

    }

    public static Distance getDistanceByCode(final DistanceCode distance) {
        switch (distance) {
            case Euclidian:
                return new Euclidian();
            case Manhatan:
                return new Manhatan();
            case Cosine:
                return new Cosine();
            case BrayCurtis:
                return new BrayCurtis();
            case Canberra:
                return new Canberra();
            case Chebyshev:
                return new Chebyshev();
            case Jaccard:
                return new Jaccard();
            case Minkowski:
                return new Minkowski();
            case PearsonCoef:
                return new PearsonCoef();
            case SorensenDice:
                return new SorensenDice();
            case SquaredEuclidian:
                return new SquaredEuclidean();
            case WuYang:
                return new WuYang();
            default:
                return new Euclidian();
        }
    }

    public static Distance getDistanceByCode(final String distance) {
        switch (distance) {
            case "Euclidian":
                return new Euclidian();
            case "Manhatan":
                return new Manhatan();
            case "Cosine":
                return new Cosine();
            case "BrayCurtis":
                return new BrayCurtis();
            case "Canberra":
                return new Canberra();
            case "Chebyshev":
                return new Chebyshev();
            case "Jaccard":
                return new Jaccard();
            case "Minkowski":
                return new Minkowski();
            case "PearsonCoef":
                return new PearsonCoef();
            case "SorensenDice":
                return new SorensenDice();
            case "SquaredEuclidian":
                return new SquaredEuclidean();
            case "WuYang":
                return new WuYang();
            default:
                return new Euclidian();
        }
    }

    private DataSet dt;

    public Distance() {
    }

    public Distance(DataSet dt) {
        this.dt = dt;
    }

    public DataSet getDt() {
        return dt;
    }

    public void setDataset(DataSet dt) {
        this.dt = dt;
    }

    public abstract double getDistance(int id, int id2);

    public abstract double getDistance(int id, double[] centroid);

    public abstract double getDistance(double[] x, double[] c);

    public double getDistance(Instance instance, double[] y) {
        double[] instList = instance.getInputNumericData();
        return getDistance(instList, y);
    }
}
