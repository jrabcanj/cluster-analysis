/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minig.distance;

import minig.data.attribute.Attribute;
import minig.data.dataset.DataSet;

/**
 *
 * @author Jaroslav
 */
public class Canberra extends Distance {

    public Canberra(DataSet dt) {
        super(dt);
    }

    public Canberra() {
    }

    @Override
    public double getDistance(int id, int id2) {
        double distance = 0;
        for (int i = 0; i < getDt().getAtributteCount(); i++) {
            Attribute attr = getDt().getAttribute(i);
            if (Attribute.isNumeric(attr)) {
                double a = (double) attr.getRow(id);
                double b = (double) attr.getRow(id2);
                double m = Math.abs(a - b);
                double c = a + b;
                distance += m / c;
            }
        }
        return distance;
    }

    @Override
    public double getDistance(int id, double[] centroid) {
        double distance = 0;
        for (int i = 0; i < centroid.length; i++) {
            Attribute attr = getDt().getAttribute(i);
            if (Attribute.isNumeric(attr)) {
                double a = centroid[i];
                double b = (double) attr.getRow(id);
                double m = Math.abs(a - b);
                double c = a + b;
                distance += m / c;
            }
        }
        return distance;
    }

    @Override
    public double getDistance(double[] a, double[] b) {
        double distance = 0;
        for (int i = 0; i < a.length; i++) {
            double m = Math.abs(a[i] - b[i]);
            double c = a[i] + b[i];
            distance += m / c;
        }
        return distance;
    }

}
