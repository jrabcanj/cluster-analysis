/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minig.distance;

import minig.data.attribute.Attribute;
import minig.data.attribute.NumericAttr;
import minig.data.dataset.DataSet;
import minig.data.dataset.instances.Instance;

/**
 *
 * @author Jaroslav
 */
public class SorensenDice extends Distance {

    public SorensenDice(DataSet dt) {
        super(dt);
    }

    public SorensenDice() {
    }

    @Override
    public double getDistance(int id, int id2) {
        double sumAB = 0, sumAp2 = 0, sumBp2 = 0;
        for (int i = 0; i < getDt().getAtributteCount(); i++) {
            Attribute attr = getDt().getAttribute(i);
            if (Attribute.isNumeric(attr)) {
                double a = (double) attr.getRow(id);
                double b = (double) attr.getRow(id2);
                sumAB += a * b;
                sumAp2 += a * a;
                sumBp2 += b * b;
            }
        }
        return getDistance(sumAp2, sumBp2, sumAB);
    }

    private double getDistance(double sumAp2, double sumBp2, double sumAB) {
        double distance;
        if (sumAp2 == 0 || sumBp2 == 0) {
            return 0;
        }
        distance = 2 * sumAB;
        distance = distance / (sumAp2 + sumBp2);
        return 1 - distance;
    }

    @Override
    public double getDistance(Instance ins, double[] centroid) {
        double sumAB = 0, sumAp2 = 0, sumBp2 = 0;
        double[] instList = ins.getInputNumericData();
        for (int i = 0; i < instList.length; i++) {
            double a = (double) instList[i];
            double b = (double) centroid[i];
            sumAB += a * b;
            sumAp2 += a * a;
            sumBp2 += b * b;
        }
        return getDistance(sumAp2, sumBp2, sumAB);
    }

    @Override
    public double getDistance(int id, double[] b) {
        double sumAB = 0, sumAp2 = 0, sumBp2 = 0;
        for (int i = 0; i < getDt().getAtributteCount(); i++) {
            Attribute attr = getDt().getAttribute(i);
            if (Attribute.isNumeric(attr)) {
                double a = (double) attr.getRow(id);
                sumAB += a * b[i];
                sumAp2 += a * a;
                sumBp2 += b[i] * b[i];
            }
        }
        return getDistance(sumAp2, sumBp2, sumAB);
    }

    @Override
    public double getDistance(double[] a, double[] b) {
        double sumAB = 0, sumAp2 = 0, sumBp2 = 0;
        for (int i = 0; i < a.length; i++) {
            sumAB += a[i] * b[i];
            sumAp2 += a[i] * a[i];
            sumBp2 += b[i] * b[i];
        }
        return getDistance(sumAp2, sumBp2, sumAB);
    }

    public static void main(String[] args) {
        DataSet dt = new DataSet();
        dt.addAttribute(new NumericAttr("1"));
        dt.addAttribute(new NumericAttr("2"));
        dt.addAttribute(new NumericAttr("3"));
        dt.addAttribute(new NumericAttr("4"));
        dt.addAttribute(new NumericAttr("5"));
        dt.addInstance(1d, 0d, 1d, 1d, 1d); //10111
        dt.addInstance(1d, 0d, 0d, 1d, 1d);
        Distance d = new Euclidian(dt);
        System.out.println(d.getDistance(0, 1));
    }

}
