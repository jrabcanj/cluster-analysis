/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minig.distance;

import minig.data.attribute.Attribute;
import minig.data.dataset.DataSet;

/**
 *
 * @author Jaroslav
 */
public class Jaccard extends Distance {

    public Jaccard(DataSet dt) {
        super(dt);
    }

    public Jaccard() {
    }

    private double getDistance(double sumAp2, double sumBp2, double sumAB) {
        double distance;
        if (sumAp2 == 0 || sumBp2 == 0) {
            return 0;
        }
        distance = sumAB / ((sumAp2 + sumBp2) - sumAB);
        return 1 - distance;
    }

    @Override
    public double getDistance(int id, double[] centroid) {
        double sumAB = 0, sumAp2 = 0, sumBp2 = 0, a, b;
        for (int i = 0; i < centroid.length; i++) {
            Attribute attr = getDt().getAttribute(i);
            if (Attribute.isNumeric(attr)) {
                a = centroid[i];
                b = (double) attr.getRow(id);
                sumAB += a * b;
                sumAp2 += a * a;
                sumBp2 += b * b;
            }
        }
        return getDistance(sumAp2, sumBp2, sumAB);
    }

    @Override
    public double getDistance(int id, int id2) {
        double sumAB = 0, sumAp2 = 0, sumBp2 = 0, a, b;
        for (int i = 0; i < getDt().getAtributteCount(); i++) {
            Attribute attr = getDt().getAttribute(i);
            if (Attribute.isNumeric(attr)) {
                a = (double) attr.getRow(id);
                b = (double) attr.getRow(id2);
                sumAB += a * b;
                sumAp2 += a * a;
                sumBp2 += b * b;
            }
        }
        return getDistance(sumAp2, sumBp2, sumAB);
    }

    @Override
    public double getDistance(double[] x, double[] c) {
        double sumAB = 0, sumAp2 = 0, sumBp2 = 0;
        for (int i = 0; i < x.length; i++) {
            sumAB += x[i] * c[i];
            sumAp2 += x[i] * x[i];
            sumBp2 += c[i] * c[i];
        }
        return getDistance(sumAp2, sumBp2, sumAB);
    }


}
