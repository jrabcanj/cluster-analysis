/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minig.distance;

import minig.data.attribute.Attribute;
import minig.data.attribute.NumericAttr;
import minig.data.dataset.DataSet;
import minig.data.dataset.instances.Instance;

/**
 *
 * @author Jaroslav
 */
public class Euclidian extends Distance {

    public Euclidian(DataSet dt) {
        super(dt);
    }

    public Euclidian() {
    }

    @Override
    public double getDistance(int id, double[] centroid) {
        double distance = 0, c, x, r;
        for (int i = 0; i < centroid.length; i++) {
            Attribute attr = getDt().getAttribute(i);
            if (Attribute.isNumeric(attr)) {
                c = centroid[i];
                x = ((NumericAttr) attr).getAttrValue().get(id);
                r = x - c;
                distance += r * r;
            }
        }
        return Math.sqrt(distance);
    }

    @Override
    public double getDistance(int id, int id2) {
        double distance = 0, c, x, r;
        for (int i = 0; i < getDt().getAtributteCount(); i++) {
            Attribute attr = getDt().getAttribute(i);
            if (Attribute.isNumeric(attr)) {
                c = (double) attr.getRow(id);
                x = (double) attr.getRow(id2);
                r = x - c;
                distance += r * r;
            }
        }
        return Math.sqrt(distance);
    }

    /**
     * Output attribute is skipped
     *
     * @param ins
     * @param d
     * @return
     */
    public double getDistance(Instance ins, double[] d) {
        double distance = 0, c, x, r;
        double[] instList = ins.getInputNumericData();
        for (int i = 0; i < instList.length; i++) {
            c = (double) instList[i];
            x = d[i];
            r = x - c;
            distance += r * r;
        }
        return Math.sqrt(distance);
    }

    @Override
    public double getDistance(double[] x, double[] c) {
        double distance = 0, r;
        for (int i = 0; i < c.length; i++) {
            r = x[i] - c[i];
            distance += r * r;
        }
        return Math.sqrt(distance);
    }

}
