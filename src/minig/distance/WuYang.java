/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minig.distance;

import application.stat.Mean;
import java.util.List;
import minig.data.attribute.Attribute;
import minig.data.attribute.NumericAttr;
import minig.data.dataset.DataSet;
import minig.data.dataset.instances.Instance;

/**
 *
 * @author Jaroslav
 */
public class WuYang extends Distance {

    private double beta;

    public WuYang(DataSet dt) {
        super(dt);
        initBeta();
    }

    public WuYang() {
    }

    @Override
    public void setDataset(DataSet dt) {
        super.setDataset(dt);
        initBeta();
    }

    private void initBeta() {
        Mean m = new Mean();
        double[] avgVector = getAvgVector();
        Euclidian dist = new Euclidian(getDt());
        for (int i = 0; i < getDt().getDataCount(); i++) {
            m.add(1 / dist.getDistance(i, avgVector));
        }
        beta = -m.get();
    }

    private double[] getAvgVector() {
        double[] vector = new double[getDt().getDatainfo().getNumAttrCount()];
        for (int i = 0; i < getDt().getDatainfo().getNumAttrCount(); i++) {
            Attribute a = getDt().getAttribute(i);
            if (Attribute.isNumeric(a)) {
                vector[i] = ((NumericAttr) a).getAttrValue().getStat().getMean();
            }
        }
        return vector;
    }

    @Override
    public double getDistance(int id, double[] centroid) {
        double distance = 0, c, x, r;
        for (int i = 0; i < centroid.length; i++) {
            Attribute attr = getDt().getAttribute(i);
            if (Attribute.isNumeric(attr)) {
                c = centroid[i];
                x = ((NumericAttr) attr).getAttrValue().get(id);
                r = x - c;
                distance += r * r;
            }
        }
        double dist = Math.sqrt(distance);
        return 1 - Math.exp(beta * dist);
    }

    @Override
    public double getDistance(int id, int id2) {
        double distance = 0, c, x, r;
        for (int i = 0; i < getDt().getAtributteCount(); i++) {
            Attribute attr = getDt().getAttribute(i);
            if (Attribute.isNumeric(attr)) {
                c = (double) attr.getRow(id);
                x = (double) attr.getRow(id2);
                r = x - c;
                distance += r * r;
            }
        }
        double dist = Math.sqrt(distance);
        return 1 - Math.exp(beta * dist);
    }

    /**
     * Output attribute is skipped
     *
     * @param ins
     * @param d
     * @return
     */
    public double getDistance(Instance ins, double[] d) {
        double distance = 0, c, x, r;
        List<Double> instList = ins.getAttrValueVectorWithoutOutputAttr();
        for (int i = 0; i < instList.size(); i++) {
            c = (double) instList.get(i);
            x = d[i];
            r = x - c;
            distance += r * r;
        }
        double dist = Math.sqrt(distance);
        return 1 - Math.exp(beta * dist);
    }

    @Override
    public double getDistance(double[] x, double[] c) {
        double distance = 0, r;
        for (int i = 0; i < c.length; i++) {
            r = x[i] - c[i];
            distance += r * r;
        }
        double dist = Math.sqrt(distance);
        return 1 - Math.exp(beta * dist);
    }

}
