/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minig.distance;

import minig.data.attribute.Attribute;
import minig.data.dataset.DataSet;

/**
 *
 * @author Jaroslav
 */
public class Manhatan extends Distance {

    public Manhatan(DataSet dt) {
        super(dt);
    }

    public Manhatan() {
    }

    @Override
    public double getDistance(int id, double[] centroid) {
        double distance = 0;
        for (int i = 0; i < centroid.length; i++) {
            Attribute attr = getDt().getAttribute(i);
            if (Attribute.isNumeric(attr)) {
                double c = centroid[i];
                double x = (double) attr.getRow(id);
                distance += Math.abs(x - c);
            }
        }
        return distance;
    }

    @Override
    public double getDistance(int id, int id2) {
        double distance = 0;
        for (int i = 0; i < getDt().getAtributteCount(); i++) {
            Attribute attr = getDt().getAttribute(i);
            if (Attribute.isNumeric(attr)) {
                double c = (double) attr.getRow(id);
                double x = (double) attr.getRow(id2);
                distance += Math.abs(x - c);
            }
        }
        return distance;
    }

    @Override
    public double getDistance(double[] x, double[] c) {
        double distance = 0;
        for (int i = 0; i < x.length; i++) {
            distance += Math.abs(x[i] - c[i]);
        }
        return distance;
    }

}
