/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minig.distance;

import minig.data.attribute.Attribute;

/**
 *
 * @author Jaroslav
 */
public class PearsonCoef extends Distance {

    @Override
    public double getDistance(int id, int id2) {
        double sumXY = 0, sumX = 0, sumY = 0, sumX2 = 0, sumY2 = 0;
        double x, y;
        int attrC = getDt().getDatainfo().getNumAttrCount();
        for (int i = 0; i < attrC; i++) {
            Attribute attr = getDt().getAttribute(i);
            if (Attribute.isNumeric(attr)) {
                x = (double) attr.getRow(id);
                y = (double) attr.getRow(id2);
                sumX += x;
                sumY += y;
                sumX2 += x * x;
                sumY2 += y * y;
                sumXY += x * y;
            }
        }
        double ret = ((attrC * sumXY) - ((sumX * sumY))) / (Math.sqrt((attrC * sumX2) - (sumX * sumX)) * Math.sqrt((attrC * sumY2) - (sumY * sumY)));
        return 1 - ret;
    }

    @Override
    public double getDistance(int id, double[] y) {
        double sumXY = 0, sumX = 0, sumY = 0, sumX2 = 0, sumY2 = 0;
        double x;
        for (int i = 0; i < y.length; i++) {
            Attribute attr = getDt().getAttribute(i);
            if (Attribute.isNumeric(attr)) {
                x = (double) attr.getRow(id);
                sumX += x;
                sumY += y[i];
                sumX2 += x * x;
                sumY2 += y[i] * y[i];
                sumXY += x * y[i];
            }
        }
        double ret = ((y.length * sumXY) - ((sumX * sumY))) / (Math.sqrt((y.length * sumX2) - (sumX * sumX)) * Math.sqrt((y.length * sumY2) - (sumY * sumY)));
        return 1 - ret;
    }

    @Override
    public double getDistance(double[] x, double[] y) {
        double sumXY = 0, sumX = 0, sumY = 0, sumX2 = 0, sumY2 = 0;
        for (int i = 0; i < x.length; i++) {
            sumX += x[i];
            sumY += y[i];
            sumX2 += x[i] * x[i];
            sumY2 += y[i] * y[i];
            sumXY += x[i] * y[i];
        }
        double ret = ((x.length * sumXY) - ((sumX * sumY))) / (Math.sqrt((x.length * sumX2) - (sumX * sumX)) * Math.sqrt((x.length * sumY2) - (sumY * sumY)));
        return 1 - ret;
    }

}
