/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minig.distance;

import java.util.List;
import minig.data.attribute.Attribute;
import minig.data.dataset.DataSet;
import minig.data.dataset.instances.NewInstance;

/**
 *
 * @author Jaroslav
 */
public class SquaredEuclidean extends Distance {

    public SquaredEuclidean(DataSet dt) {
        super(dt);
    }

    public SquaredEuclidean() {
    }

    @Override
    public double getDistance(int id, double[] centroid) {
        double distance = 0, c, x, r;
        for (int i = 0; i < centroid.length; i++) {
            Attribute attr = getDt().getAttribute(i);
            if (Attribute.isNumeric(attr)) {
                c = centroid[i];
                x = (double) attr.getRow(id);
                r = x - c;
                distance += r * r;
            }
        }
        return distance;
    }

    @Override
    public double getDistance(double[] x, double[] c) {
        double distance = 0, r;
        for (int i = 0; i < c.length; i++) {
            r = x[i] - c[i];
            distance += r * r;
        }
        return distance;
    }

    @Override
    public double getDistance(int id, int id2) {
        double distance = 0, c, x, r;
        for (int i = 0; i < getDt().getAtributteCount(); i++) {
            Attribute attr = getDt().getAttribute(i);
            if (Attribute.isNumeric(attr)) {
                c = (double) attr.getRow(id);
                x = (double) attr.getRow(id2);
                r = x - c;
                distance += r * r;
            }
        }
        return distance;
    }

    /**
     * Output attribute is skipped
     *
     * @param ins
     * @param d
     * @return
     */
    public double getDistance(NewInstance ins, double[] d) {
        double distance = 0, c, x, r;
        List<Double> instList = ins.getAttrValueVectorWithoutOutputAttr();
        for (int i = 0; i < instList.size(); i++) {
            c = (double) instList.get(i);
            x = d[i];
            r = x - c;
            distance += r * r;
        }
        return distance;
    }

}
