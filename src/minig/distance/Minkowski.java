/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minig.distance;

import minig.data.attribute.Attribute;
import minig.data.dataset.DataSet;

/**
 *
 * @author Jaroslav
 */
public class Minkowski extends Distance {

    private double pow = 2;

    public Minkowski(DataSet dt) {
        super(dt);
    }

    public Minkowski() {
    }

    public Minkowski(double pow) {
        this.pow = pow;
    }

    public double getPower() {
        return pow;
    }

    public void setPower(double pow) {
        this.pow = pow;
    }

    @Override
    public double getDistance(double[] x, double[] c) {
        double distance = 0;
        for (int i = 0; i < x.length; i++) {
            distance += Math.pow(Math.abs(c[i] - x[i]), pow);
        }
        return Math.pow(distance, 1 / pow);
    }

    @Override
    public double getDistance(int id, int id2) {
        double distance = 0;
        for (int i = 0; i < getDt().getAtributteCount(); i++) {
            Attribute attr = getDt().getAttribute(i);
            if (Attribute.isNumeric(attr)) {
                double c = (double) attr.getRow(id);
                double x = (double) attr.getRow(id2);
                distance += Math.pow(Math.abs(c - x), pow);
            }
        }
        return Math.pow(distance, 1 / pow);
    }

    @Override
    public double getDistance(int id, double[] x) {
        double distance = 0;
        for (int i = 0; i < x.length; i++) {
            Attribute attr = getDt().getAttribute(i);
            if (Attribute.isNumeric(attr)) {
                double c = (double) attr.getRow(id);
                distance += Math.pow(Math.abs(c - x[i]), pow);
            }
        }
        return Math.pow(distance, 1 / pow);
    }

}
