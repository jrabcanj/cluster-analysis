/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minig.clustering;

import java.util.List;

/**
 *
 * @author Jaroslav
 */
public interface Cluster {

    /**
     * Centers, centroids or whatever
     *
     * @return
     */

    public double[] getCentroids();

    public List<Double> getCentroidInList();

    public abstract List<Integer> getIds();
}
