/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minig.clustering;

import application.ConsolePrintable;
import application.ProjectUtils;
import application.structures.DoubleVector;
import minig.data.dataset.DataSet;
import minig.data.dataset.instances.NewInstance;

/**
 *
 * @author Jaroslav
 */
public interface SoftClustering extends Clustering {

    public abstract DoubleVector[] getPartitions();

    public double[] classify(NewInstance i);

    public DataSet getDataset();

    default void printPartitions() {
        for (int i = 0; i < getDataset().getDataCount(); i++) {
            ConsolePrintable.print(i + ".  ");
            for (int j = 0; j < getPartitions().length; j++) {
                ConsolePrintable.print(ProjectUtils.formatDouble(getPartitions()[j].get(i)) + "  ");
            }
            if (getDataset().hasOutputAttr()) {
                ConsolePrintable.print(getDataset().getOutputAttr().getRowString(i) + "  ");
            }
            ConsolePrintable.newLine();
        }
    }

    default void printPartition(int i) {
            ConsolePrintable.print(i + ".  ");
            for (int j = 0; j < getPartitions().length; j++) {
                ConsolePrintable.print(ProjectUtils.formatDouble(getPartitions()[j].get(i)) + "  ");
            }
            if (getDataset().hasOutputAttr()) {
                ConsolePrintable.print(getDataset().getOutputAttr().getRowString(i) + "  ");
            }
            ConsolePrintable.newLine();
    }
}
