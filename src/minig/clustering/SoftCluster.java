/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minig.clustering;

import application.structures.DoubleVector;
import java.util.List;

/**
 *
 * @author Jaroslav
 */
public interface SoftCluster extends Cluster {

    public DoubleVector getPartitions();

    default List<Integer> getIds() {
        throw new UnsupportedOperationException("In case of fuzzy clustering, each instance belongs to each cluster with some grade of membership. Therefore, it is necessary to use partitions instead of ids"); //To change body of generated methods, choose Tools | Templates.
    }

}
