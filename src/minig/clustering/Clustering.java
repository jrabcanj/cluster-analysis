/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minig.clustering;

import minig.data.dataset.DataSet;
import application.ConsolePrintable;
import java.util.List;
import minig.distance.Distance;

/**
 *
 * @author Jaroslav
 */
public interface Clustering extends ConsolePrintable {

    /**
     * method starts clustering
     */
    public void doClustering();

    /**
     * method starts clustering
     */
    public default void buildModel() {
        doClustering();
    }

    public <T extends Cluster> List<T> getClusters();

    public void setDataset(DataSet dt);

    /**
     *
     * @return number of clusters after build of model
     */
    public int getClusterCount();

    /**
     * Method set type of distance metric, for example Euclidian
     *
     * @param dist class specifies distance metrics
     */
    public void setDistance(Distance dist);

    public Distance getDistance();

}
