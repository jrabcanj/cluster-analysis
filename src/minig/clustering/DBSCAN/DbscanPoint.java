/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minig.clustering.DBSCAN;

import minig.data.dataset.DataSet;
import minig.data.dataset.instances.DatasetInstance;

/**
 *
 * @author Jaroslav
 */
public class DbscanPoint {

    private Status status;
    private DatasetInstance instance;
    private int neighbors = 0;
    private boolean assigned = false;

    public enum Status {
        CORE, NOISE, BORDER
    }

    public DbscanPoint(int index, DataSet dataSet) {
        this.instance = dataSet.getLinkedInstance(index);
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public DatasetInstance getInstance() {
        return instance;
    }

    public void setInstance(DatasetInstance instance) {
        this.instance = instance;
    }
    
    public int getNeighborsCount() {
        return this.neighbors;
    }
    
    public void addNeighborsCount() {
        this.neighbors++;
    }
    
    public boolean isAssigned() {
        return this.assigned;
    }
    
    public void setAssigned() {
        this.assigned = true;
    }
    
    private String dataToStirng() {
        String result = "";
        for (double d : instance.getNumericData()) {
            result += d + " ";
        }
        return result;
    }

    @Override
    public String toString() {
        return dataToStirng();
    }

}
