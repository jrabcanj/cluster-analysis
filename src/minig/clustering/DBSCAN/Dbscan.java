/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minig.clustering.DBSCAN;

import application.ConsolePrintable;
import application.ProjectUtils;
import application.StopWatch;
import minig.clustering.GeneralCluster;
import java.util.ArrayList;
import minig.clustering.Clustering;
import minig.data.dataset.DataSet;
import minig.distance.Distance;
import minig.distance.DistanceBased;
import minig.distance.Euclidian;

/**
 *
 * @author Jaroslav
 */
public class Dbscan implements Clustering, ConsolePrintable, DistanceBased, Runnable {

    private boolean done = false;
    private DataSet dataSet;
    private Distance distance = new Euclidian();
    private final int minPoints;
    private final double maxRadius;
    private static int defaultMinPoints = 5;
    private static double defaultMaxRadius = 1;
    private ArrayList<DbscanCluster> clusters = new ArrayList<>();
    private ArrayList<DbscanPoint> points = new ArrayList<>();
    private StopWatch stopWatch = new StopWatch();
    private Thread progressThread; 

    public Dbscan(DataSet ds) {
        setDataset(ds);
        this.minPoints = defaultMinPoints;
        this.maxRadius = defaultMaxRadius;
    }

    public Dbscan(DataSet ds, int minCount, double maxDistance) {
        this.dataSet = ds;
        this.distance.setDataset(dataSet);
        this.minPoints = minCount;
        this.maxRadius = maxDistance;
    }
    public void run() {
        System.out.println("Run thread");
        try {
            while (!done) {
                getProcessInfo();
                Thread.sleep(10000);
            }
        } catch (InterruptedException e) {
            System.out.println("Thread was interrupted.");
        }
        System.out.println("Thread exiting.");
    }
    
    private void getProcessInfo() {
        int pointProcessed = this.dataSet.getDataCount() - this.points.size();
        int pointLeft = this.points.size();
        double timeLeft = (this.stopWatch.getCurrentTime() / pointProcessed) * pointLeft;
        int[] getTime = ProjectUtils.secondsToTime(timeLeft);
        System.out.println("Time: " + getTime[0] + ":" + getTime[1] + ":" + getTime[2]);
        System.out.println("Points processed: " + pointProcessed);
        System.out.println("Points left: " + pointLeft);
    }

    public DataSet getDataSet() {
        return this.dataSet;
    }

    @Override
    public void doClustering() {        
        this.distance.setDataset(dataSet);
        DbscanCluster emptyCluster = new DbscanCluster(distance, minPoints, maxRadius);
        
        for (int i = 0; i < this.dataSet.getDataCount(); i++) {
            DbscanPoint point = new DbscanPoint(i, dataSet);
            points.add(point);
        }
        
        this.stopWatch = new StopWatch();
        this.progressThread = new Thread(this);
        this.progressThread.start();
        
        for (int i = 0; i < points.size(); i++) {
            DbscanPoint point = points.get(i);
            if (point.getStatus() != null) continue;
            
            ArrayList<DbscanPoint> neighbors = emptyCluster.getNeighbors(point, points);
            if (neighbors.size() <= this.minPoints) {
                point.setStatus(DbscanPoint.Status.NOISE);
                points.remove(i--);
                continue;
            }
            
            DbscanCluster cluster = new DbscanCluster(distance, minPoints, maxRadius);
            point.setStatus(DbscanPoint.Status.CORE);
            cluster.getPoints().add(point);
            cluster.expandCluster(neighbors, points);
            this.clusters.add(cluster);
            i--;
        }
        
        done = true;
    }
    
    @Override
    public ArrayList<DbscanCluster> getClusters() {
        return this.clusters;
    }

    @Override
    public final void setDataset(DataSet dt) {
        this.dataSet = dt;
        this.distance.setDataset(dataSet);
    }

    @Override
    public int getClusterCount() {
        return this.clusters.size();
    }

    @Override
    public void setDistance(Distance dist) {
        this.distance = dist;
    }

    @Override
    public Distance getDistance() {
        return this.distance;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (DbscanCluster cluster : clusters) {
            sb.append(cluster.toString()).append(System.lineSeparator());
        }
        return sb.toString();
    }
    
    // transform clusters and all data to one union class GENERAL CLUSTER
    public ArrayList<GeneralCluster> getGeneralClusterData() {
        GeneralCluster cluster;
        ArrayList<GeneralCluster> genClusters = new ArrayList<>();
        
        for (DbscanCluster c : this.clusters) {
            cluster = new GeneralCluster();
            for (DbscanPoint p : c.getPoints()) {
                cluster.getPoints().add(p.getInstance());
            }
            genClusters.add(cluster);
        }
        return genClusters;
    }

}
