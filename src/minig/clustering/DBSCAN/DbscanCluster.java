/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minig.clustering.DBSCAN;

import java.util.List;
import java.util.ArrayList;
import minig.distance.Distance;

/**
 *
 * @author Jaroslav
 */
public class DbscanCluster {

    private final int minPoints;
    private final double maxRadius;

    private Distance distance;
    private ArrayList<DbscanPoint> points;

    public DbscanCluster(Distance distance, int minPoints, double maxRadius) {
        this.distance = distance;
        this.minPoints = minPoints;
        this.maxRadius = maxRadius;
        this.points = new ArrayList<>();
    }

    public void expandCluster(ArrayList<DbscanPoint> clusterPoints, ArrayList<DbscanPoint> allPoints) {
        for (int i = 0; i < clusterPoints.size(); i++) { 
            
            DbscanPoint currentPoint = clusterPoints.get(i);
            DbscanPoint.Status status = currentPoint.getStatus();
            allPoints.remove(currentPoint);
            
            if (status == DbscanPoint.Status.NOISE) {
                currentPoint.setStatus(DbscanPoint.Status.BORDER);            
                this.points.add(currentPoint);
                continue;
            }
            
            if (status != null) { continue; }
            
            currentPoint.setStatus(DbscanPoint.Status.CORE);
            this.points.add(currentPoint);
            
            ArrayList<DbscanPoint> currentNeighbors = getNeighbors(currentPoint, allPoints);
            if (currentPoint.getNeighborsCount() >= minPoints) {
                clusterPoints.addAll(currentNeighbors);
            }
            
        }
    }

    public ArrayList<DbscanPoint> getNeighbors(DbscanPoint point, ArrayList<DbscanPoint> points) {
        int index = point.getInstance().getIndex();
        ArrayList<DbscanPoint> neighbors = new ArrayList<>();
        for (int i = 0; i < points.size(); i++) {
            DbscanPoint neighbor = points.get(i);
            int inexOfNeighbor = neighbor.getInstance().getIndex();
            double dist = this.distance.getDistance(index, inexOfNeighbor);
            if (dist <= this.maxRadius) {
                neighbor.addNeighborsCount();
                point.addNeighborsCount();
                if (!neighbor.isAssigned()) {
                    neighbor.setAssigned();
                    neighbors.add(neighbor);
                }
            }
        }
        return neighbors;
    }

    public List<DbscanPoint> getPoints() {
        return points;
    }

    public String getPointsAsString() {
        String result = "";
        for (int i = 0; i < points.size(); i++) {
            result += points.get(i) + " ";
        }
        return result;
    }

    @Override
    public String toString() {
        return "Cluster size: " + points.size() + "\n"
                + "Points: " + points.toString();
    }

}
