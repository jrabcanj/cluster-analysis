/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minig.clustering.OPTICS;

import application.ConsolePrintable;
import application.ProjectUtils;
import application.StopWatch;
import java.util.ArrayList;
import java.util.PriorityQueue;
import minig.clustering.Clustering;
import minig.clustering.Coordinates;
import minig.clustering.GeneralCluster;
import minig.data.dataset.DataSet;
import minig.distance.Distance;
import minig.distance.DistanceBased;
import minig.distance.Euclidian;

/**
 *
 * @author Jaroslav
 */
public class Optics implements Clustering, ConsolePrintable, DistanceBased, Runnable {
        
    private final int defaultMinPoints = 5;
    private final double defaultMaxRadius = 1;
    
    private int minPoints;
    private double maxRadius;
    private DataSet dataSet;
    private Distance distance = new Euclidian();
    
    private PriorityQueue<OpticsPoint> seeds = new PriorityQueue<OpticsPoint>();
    private ArrayList<OpticsPoint> orderedStructure = new ArrayList<>();
    private ArrayList<GeneralCluster> clusters = new ArrayList<>();
    private ArrayList<OpticsPoint> points = new ArrayList<>();
    
    private StopWatch stopWatch = new StopWatch();
    private Thread progressThread; 
    public static Optics Instance;
    
    public Optics() {
        this.Instance = this;
    }
    
    public Optics(DataSet ds) {
        this();
        this.minPoints = defaultMinPoints;
        this.maxRadius = defaultMaxRadius;
        setDataset(ds);
    }

    public Optics(DataSet ds, int minCount, double maxDistance) {
        this();
        this.minPoints = minCount;
        this.maxRadius = maxDistance;
        setDataset(ds);
    }
    
    public DataSet getDataSet() {
        return dataSet;
    }

    public void setDataSet(DataSet dataSet) {
        this.dataSet = dataSet;
    }

    public int getMinPoints() {
        return minPoints;
    }

    public void setMinPoints(int minPoints) {
        this.minPoints = minPoints;
    }

    public double getMaxRadius() {
        return maxRadius;
    }

    public void setMaxRadius(double maxRadius) {
        this.maxRadius = maxRadius;
    }
    
    public ArrayList<OpticsPoint> getOrderedStructure() {
        return this.orderedStructure;
    }
    
    public ArrayList<Double> getOrderedStructureOfDistances() {
        ArrayList<Double> distances = new ArrayList<>();
        for (int i = 0; i < orderedStructure.size(); i++) {
            distances.add(orderedStructure.get(i).getReachDistanne());
        }
        return distances;
    }
    
    public void run() {
        System.out.println("Run thread");
        try {
            while (this.points.size() > 1) {
                getProcessInfo();
                Thread.sleep(10000);
            }
        } catch (InterruptedException e) {
            System.out.println("Thread was interrupted.");
        }
        System.out.println("Thread exiting.");
    }
    
    private void getProcessInfo() {
        int pointProcessed = this.orderedStructure.size();
        int pointLeft = this.points.size();
        double timeLeft = (this.stopWatch.getCurrentTime() / pointProcessed) * pointLeft;
        int[] getTime = ProjectUtils.secondsToTime(timeLeft);
        System.out.println("Time: " + getTime[0] + ":" + getTime[1] + ":" + getTime[2]);
        System.out.println("Points processed: " + pointProcessed);
        System.out.println("Points left: " + pointLeft);
    }
    
    public void expandClusterOrder(OpticsPoint currentPoint) {
        ArrayList<OpticsPoint> neighbors = getNeighbors(currentPoint, this.points);
        currentPoint.setProcessed(true);
        currentPoint.calculateCoreDistance(neighbors, maxRadius, minPoints);

        this.orderedStructure.add(currentPoint);
        this.points.remove(currentPoint);

        if (currentPoint.getCoreDistance() != -1) {
            GeneralCluster currCluster = new GeneralCluster();
            this.clusters.add(currCluster);
            currCluster.getPoints().add(currentPoint.getInstance());
            
            update(neighbors, currentPoint);
            
            while (!seeds.isEmpty()) {
                OpticsPoint neighPoint = seeds.poll();
                neighPoint.calculateCoreDistance(neighbors, maxRadius, minPoints);
                neighPoint.setProcessed(true);
                currCluster.getPoints().add(neighPoint.getInstance());
                this.orderedStructure.add(neighPoint);
                this.points.remove(neighPoint);
                neighbors = getNeighbors(neighPoint, this.points);
                
                if (neighPoint.getCoreDistance() != -1) {
                    update(neighbors, neighPoint);
                }
            }
        }
    }
    
    public void update(ArrayList<OpticsPoint> neighbors, OpticsPoint centralPoint) {
        double cPointDist = centralPoint.getCoreDistance();
        for (OpticsPoint point : neighbors) {
            if (!point.isProcessed()) {
                double distBetween = distance.getDistance(centralPoint.getInstance().getIndex(), point.getInstance().getIndex());
                double newReachDist = Math.max(cPointDist, distBetween);
                if (point.getReachDistanne() == -1) {
                    point.setReachDistanne(newReachDist);
                    seeds.add(point);
                } else if (newReachDist < point.getReachDistanne()) {
                    point.setReachDistanne(newReachDist);
                    seeds.remove(point);
                    seeds.add(point);
                }
            }
        }
    }
    
    public ArrayList<OpticsPoint> getNeighbors(OpticsPoint point, ArrayList<OpticsPoint> points) { 
        int index = point.getInstance().getIndex();
        ArrayList<OpticsPoint> neighbors = new ArrayList<>();
        for (int i = 0; i < points.size(); i++) {
            OpticsPoint neighbor = points.get(i);
            int inexOfNeighbor = neighbor.getInstance().getIndex();
            double dist = getDistance().getDistance(index, inexOfNeighbor);
            if (dist <= this.maxRadius) {
                neighbors.add(points.get(i));
            }
        }
        return neighbors;
    }
    
    @Override
    public void doClustering() {
        this.stopWatch = new StopWatch();
        this.points = new ArrayList<>();
        for (int i = 0; i < dataSet.getDataCount(); i++) {
            this.points.add(new OpticsPoint(i, dataSet.getLinkedInstance(i)));
        }

        this.progressThread = new Thread(this);
        this.progressThread.start();
        
        for (int i = 0; i < this.points.size(); i++) {
            OpticsPoint point = this.points.get(i);
            if (!point.isProcessed()) {
                expandClusterOrder(point);
                i = 0;
            }
        }
        
        /*
        
        
                if (this.clusters.get(this.clusters.size() - 1).getPoints().size() > this.minPoints) {
                    OpticsPoint point0 = this.orderedStructure.get(orderedStructure.size() - 3);
                    OpticsPoint point1 = this.orderedStructure.get(orderedStructure.size() - 2);
                    
                    double diffPoints = point1.getReachDistanne() - point0.getReachDistanne();
                    double diffActual = point1.getReachDistanne() - neighPoint.getReachDistanne();
                    
                    if (diffPoints > 0 && diffActual > 0 && diffPoints < diffActual) {
                        GeneralCluster newCluster = new GeneralCluster();
                        this.clusters.add(newCluster);
                    }
                }
        
        */
    }

    @Override
    public final void setDataset(DataSet dt) {
        setDataSet(dt);
        getDistance().setDataset(getDataSet());
    }

    @Override
    public void setDistance(Distance dist) {
         this.distance = dist;
    }

    @Override
    public Distance getDistance() {
        return this.distance;
    }
    
    @Override
    public void print() {
        System.out.println("Clusters:");
        for (int i = 0; i < this.clusters.size(); i++) {
            
            if (clusters.get(i).getPoints().size() == 1) { continue; }
            
            System.out.print("Cluster " + i + " size " + this.clusters.get(i).getPoints().size() + ": ");
            System.out.println(this.clusters.get(i).toString());
        }
    }
    
    public ArrayList<String> getClusterPointsCoordinates() {
        ArrayList<String> outPut = new ArrayList<>();
        for (int i = 0; i < this.clusters.size(); i++) {
            if (clusters.get(i).getPoints().size() == 1) { continue; }
            System.out.print("Cluster n." + i + ": ");
            ArrayList<Coordinates> coor = this.clusters.get(i).getPointsCoordinates(150, 100);
            for (Coordinates c : coor) {
                System.out.print(c.toString());
            }
            System.out.println("");
        }
        return outPut;
    }

    @Override
    public ArrayList<GeneralCluster> getClusters() {
        return this.clusters;
    }

    @Override
    public int getClusterCount() {
        return this.clusters.size();
    }
    
}