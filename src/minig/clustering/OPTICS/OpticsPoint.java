/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minig.clustering.OPTICS;

import java.util.ArrayList;
import java.util.Comparator;
import minig.data.dataset.instances.DatasetInstance;

/**
 *
 * @author Jaroslav
 */
public class OpticsPoint implements Comparator<OpticsPoint>, Comparable<OpticsPoint> {
    
    private int index;
    private boolean processed;
    private double coreDistance;
    private double reachDistance;
    private DatasetInstance instance;

    public OpticsPoint(int index, DatasetInstance instance) {
        this.index = index;
        this.instance = instance;
        this.coreDistance = -1;
        this.reachDistance = -1;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public DatasetInstance getInstance() {
        return instance;
    }

    public void setInstance(DatasetInstance instance) {
        this.instance = instance;
    }

    public boolean isProcessed() {
        return processed;
    }

    public void setProcessed(boolean processed) {
        this.processed = processed;
    }

    public double getCoreDistance() {
        return coreDistance;
    }

    public void setCoreDistance(double dist) {
        this.coreDistance = dist;
    }

    public double getReachDistanne() {
        return reachDistance;
    }

    public void setReachDistanne(double reachDistanne) {
        this.reachDistance = reachDistanne;
    }
    
    public void setReachDistance(OpticsPoint point, ArrayList<OpticsPoint> neighbors) {
        double newReachDist = 0;
        double cPointDist = point.getCoreDistance();
        for (OpticsPoint neighbor : neighbors) {
            newReachDist = Math.max(cPointDist, Optics.Instance.getDistance().getDistance(point.getInstance().getIndex(), neighbor.getInstance().getIndex()));
        }
        point.setReachDistanne(newReachDist);
    }

    public void calculateCoreDistance(ArrayList<OpticsPoint> neighbors, double maxRadius, int minPoints) {
        if (neighbors.size() <= Optics.Instance.getMinPoints()) {
            this.setCoreDistance(-1);
        } else {
            // SortPointsByDistance(neighbors);
            // double distance = Optics.Instance.getDistance().getDistance(instance.getIndex(), neighbors.get(minPoints - 1).getInstance().getIndex());
            
            double distance = getNearestPointDistance(neighbors);
            this.setCoreDistance(distance);
        }
    }

    @Override
    public int compare(OpticsPoint t, OpticsPoint t1) {
        if (t.getReachDistanne() < t1.getReachDistanne()) return 1;
        else if (t.getReachDistanne() > t1.getReachDistanne()) return -1;
        return 0;
    }
    
    @Override
    public int compareTo(OpticsPoint t) {
        if (t.getReachDistanne() < reachDistance) return 1;
        else if (t.getReachDistanne() > reachDistance) return -1;
        return 0;
    }
    
    private String getNumericDataAsString() {
        String data = "";
        for (double d : instance.getNumericData()) {
            data += d + " ";
        }
        return data;
    }

    @Override
    public String toString() {
        return getNumericDataAsString() + ", " + reachDistance;
    }

    private void SortPointsByDistance(ArrayList<OpticsPoint> neighbors) {
        boolean swap = true;
        while (swap) {
            swap = false;
            for (int i = 0; i < neighbors.size() - 1; i++) {
                double dist1 = Optics.Instance.getDistance().getDistance(instance.getIndex(), neighbors.get(i).getInstance().getIndex());
                double dist2 = Optics.Instance.getDistance().getDistance(instance.getIndex(), neighbors.get(i + 1).getInstance().getIndex());
                if (dist1 > dist2) {
                    OpticsPoint newPoint = neighbors.get(i);
                    neighbors.set(i, neighbors.get(i + 1));
                    neighbors.set(i + 1, newPoint);
                    swap = true;
                }
            }
        }
    }
    
    private double getNearestPointDistance(ArrayList<OpticsPoint> points) {
        double min = Double.MAX_VALUE;
        for (int i = 0; i < points.size(); i++) {
            double dist = Optics.Instance.getDistance().getDistance(instance.getIndex(), points.get(i).getInstance().getIndex());
            if (dist < min) {
                min = dist;
            }
        }
        return min;
    }
}