/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minig.clustering.kmeans;

import application.ProjectUtils;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import minig.clustering.Cluster;
import minig.data.attribute.Attribute;
import minig.data.dataset.DataSet;
import minig.data.dataset.instances.NewInstance;

/**
 *
 * @author Jaroslav
 */
public class MeansCluster implements Cluster {

    private DataSet dt;

    private double[] centroids;
    private double[] nextCentroids;

    private LinkedList<Integer> ids = new LinkedList<>();

    public MeansCluster(NewInstance ins, DataSet dt) {
        super();
        centroids = new double[dt.getDatainfo().getNumAttrCount()];
        nextCentroids = new double[dt.getDatainfo().getNumAttrCount()];
        List<Object> o = ins.getFeatureVector();
        int count = 0;
        for (int i = 0; i < dt.getAtributteCount(); i++) {
            Attribute attr = dt.getAttribute(i);
            if (Attribute.isNumeric(attr)) {
                centroids[count++] = (Double) o.get(i);
            }
        }
        this.dt = dt;
    }

    public DataSet getDt() {
        return dt;
    }

    public LinkedList<Integer> getIds() {
        return ids;
    }

    public double[] getCentroids() {
        return centroids;
    }

    public double updateCenter() {
        double diff = 0;
        if (!ids.isEmpty()) {
            for (int j = 0; j < centroids.length; j++) {
                double newCentroid = nextCentroids[j] / ids.size();
                diff += Math.abs(centroids[j] - newCentroid);
                centroids[j] = newCentroid;
                nextCentroids[j] = 0;
            }
        }
        return diff;
    }

    public void clearIds() {
        ids.clear();
    }

    @Override
    public String toString() {
        return "centroids=" + ProjectUtils.doulbeArrayToString(centroids) + "   size: " + ids.size();
    }

    synchronized void add(int index) {
        ids.add(index);
        for (int i = 0; i < dt.getDatainfo().getNumAttrCount(); i++) {
            Attribute attr = dt.getAttribute(i);
            if (Attribute.isNumeric(attr)) {
                double s = ((double) attr.getRow(index));
                nextCentroids[i] += s;
            }
        }
    }

    @Override
    public List<Double> getCentroidInList() {
        List<Double> r = new ArrayList<>(centroids.length);
        for (double centroid : centroids) {
            r.add(centroid);
        }
        return r;
    }

}
