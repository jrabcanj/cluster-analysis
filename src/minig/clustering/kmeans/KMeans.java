/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minig.clustering.kmeans;

import java.util.ArrayList;
import java.util.List;
import minig.clustering.Clustering;
import minig.distance.Distance;
import minig.distance.Euclidian;
import minig.data.dataset.DataSet;
import application.ConsolePrintable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.logging.Level;
import java.util.logging.Logger;
import minig.clustering.GeneralCluster;
import minig.data.dataset.instances.DatasetInstance;
import minig.distance.DistanceBased;

/**
 *
 * @author Jaroslav
 */
public class KMeans implements Clustering, ConsolePrintable, DistanceBased {

    private DataSet dt;
    private Distance distance = new Euclidian();
    private List<MeansCluster> clusters;
    private int k;
    private int maxItertionCount = 1000;
    private double treshold = 0.00001;
    private int[] intervals;
    private int threadCount = 8;

    private static int defaultClusterCount = 2;

    public KMeans(DataSet dt) {
        this(defaultClusterCount);
        distance = new Euclidian(dt);
        this.dt = dt;
    }

    public KMeans(int k) {
        this.k = k;
        clusters = new ArrayList(k);
    }

    public void setThreadCount(int threadCount) {
        this.threadCount = threadCount;
    }

    public int getClusterCount() {
        return k;
    }

    public KMeans(DataSet dt, int k) {
        this(k);
        this.dt = dt;
    }

    public KMeans() {
        this(defaultClusterCount);
    }

    public void setDistance(Distance distance) {
        this.distance = distance;
    }

    public Distance getDistance() {
        return distance;
    }

    public void setNumberOfClusters(int k) {
        this.k = k;
    }

    public void setTreshold(double treshold) {
        this.treshold = treshold;
    }

    public List<MeansCluster> getClusters() {
        return clusters;
    }

    public double getTreshold() {
        return treshold;
    }

    public DataSet getDataset() {
        return dt;
    }

    public void setDataset(DataSet dt) {
        this.dt = dt;
    }

    public void initIntervals() {
        intervals = new int[threadCount + 1];
        int step = getDataset().getDataCount() / (threadCount);
        int pom = 0;
        int i;
        for (i = 0; i < threadCount; i++) {
            intervals[i] = pom;
            pom += step;
        }
        intervals[i] = getDataset().getDataCount();
        //System.out.println(intervals);
    }

    public void doClustering() {
        initIntervals();
        ThreadPoolExecutor executor = new ScheduledThreadPoolExecutor(threadCount);
        reinit();
        initClusters();
        distance.setDataset(getDataset());
        int limit = maxItertionCount;
        double diff;

        do {
            clearIds();
            CountDownLatch cdl = new CountDownLatch(threadCount);
            for (int i = 0; i < intervals.length - 1; i++) {
                int x1 = intervals[i];
                int x2 = intervals[i + 1];
                Runnable r = () -> {
                    for (int i1 = x1; i1 < x2; i1++) {
                        double minimalDistance = Double.MAX_VALUE;
                        MeansCluster minCluster = null;
                        for (int j = 0; j < clusters.size(); j++) {
                            MeansCluster cls = clusters.get(j);
                            double dis = distance.getDistance(i1, cls.getCentroids());
                            if (minimalDistance > dis) {
                                minimalDistance = dis;
                                minCluster = cls;
                            }
                        }
                        minCluster.add(i1);
                    }
                    cdl.countDown();
                };
                executor.execute(r);
            }
            try {
                cdl.await();
            } catch (InterruptedException ex) {
                Logger.getLogger(KMeans.class.getName()).log(Level.SEVERE, null, ex);
            }
            diff = updateCenters();
        } while (limit-- > 0 && diff - treshold > 0);

        executor.shutdown();
    }

    public int getMaxItertionCount() {
        return maxItertionCount;
    }

    public void setMaxRuns(int maxItertionCount) {
        this.maxItertionCount = maxItertionCount;
    }

    //--------------------------------------
    private double updateCenters() {
        double diff = 0;
        for (MeansCluster cluster : clusters) {
            diff += cluster.updateCenter();
        }
        return diff;
    }

    private void clearIds() {
        for (MeansCluster cluster : clusters) {
            cluster.clearIds();
        }
    }

    private void initClusters() {
        int step = dt.getDataCount() / k;
        if (step < 1) {
            step = 1;
        }
        int insIndex = 0;
        for (int i = 0; i < k; i++) {
            MeansCluster km = new MeansCluster(dt.getInstance(insIndex), dt);
            clusters.add(km);
            insIndex += step;
            if (dt.getDataCount() - 1 < insIndex) {
                insIndex %= dt.getDataCount();
            }
        }
    }

    public void reinit() {
        clusters.clear();
    }
    
    // transform clusters and all data to one union class GENERAL CLUSTER
    public ArrayList<GeneralCluster> getGeneralClusterData() {
        GeneralCluster cluster;
        ArrayList<GeneralCluster> genClusters = new ArrayList<>();
        
        for (MeansCluster c : this.clusters) {
            cluster = new GeneralCluster();
            for (int p : c.getIds()) {
                cluster.getPoints().add(this.dt.getLinkedInstance(p));
            }
            genClusters.add(cluster);
        }
        return genClusters;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("");
        for (MeansCluster cluster : clusters) {
            sb.append(cluster.toString()).append(System.lineSeparator());
        }
        return sb.toString();
    }

}
