/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minig.clustering;

import java.util.ArrayList;
import java.util.List;
import minig.data.dataset.DataSet;
import minig.data.dataset.instances.DatasetInstance;

/**
 *
 * @author Jaroslav
 */
public class GeneralCluster {
    
    private List<DatasetInstance> points;
    private float[] clusterColor;
    private double[] centroid;
    private double eps;
    
    public GeneralCluster() {
        points = new ArrayList<>();
    }
    
    public GeneralCluster(List<DatasetInstance> listOfPoints) {
        points = listOfPoints;
    }
    
    public List<DatasetInstance> getPoints() {
        return this.points;
    }
    
    public double[] getCentroid() {
        // if empty list of point retun 0
        if (points.isEmpty()) return null;
        if (this.centroid != null) return this.centroid;
        
        int dataLentgth = points.get(0).getNumericData().length;
        double[] centroid = new double[dataLentgth];
        
        for (int i = 0; i < centroid.length; i++) 
            centroid[i] = 0;
        
        for (DatasetInstance point : points) {
            for (int i = 0; i < dataLentgth; i++) {
                centroid[i] += point.getNumericData()[i];
            }
        }
        
        for (int i = 0; i < centroid.length; i++) 
            centroid[i] = centroid[i] / points.size();
        
        this.centroid = centroid;
        return this.centroid;
    }
    
    public static double[] getDatasetCentroid(DataSet dataset) {
        int dataLentgth = dataset.getDatasetInstances().get(0).getNumericData().length;
        double[] centroid = new double[dataLentgth];
        
        for (int i = 0; i < centroid.length; i++) 
            centroid[i] = 0;
        
        for (DatasetInstance point : dataset.getDatasetInstances()) {
            for (int i = 0; i < dataLentgth; i++) {
                centroid[i] += point.getNumericData()[i];
            }
        }
        
        for (int i = 0; i < centroid.length; i++) 
            centroid[i] = centroid[i] / dataset.getDatasetInstances().size();
        
        return centroid;
    }
    
    public void setClusterColor() {
        double red = 0.0;
        double green = 0.0;
        double blue = 0.0;
        float[] rgbColor = new float[3];
        
        for (int i = 0; i < this.points.size(); i++) {
           red += this.points.get(i).getNumericData()[0];
           green += this.points.get(i).getNumericData()[1];
           blue += this.points.get(i).getNumericData()[2];
        }
        
        rgbColor[0] = (float) red / this.points.size();
        rgbColor[1] = (float) green / this.points.size();
        rgbColor[2] = (float) blue / this.points.size();
        
        this.clusterColor = rgbColor;
    }
    
    public float[] getClusterColor() {
        return this.clusterColor;
    }
    
    public ArrayList<Coordinates> getPointsCoordinates(int width, int height) {
        ArrayList<Coordinates> coor = new ArrayList<>();
        for (DatasetInstance point : points) {
            coor.add(getPointCoordinates(point, width));
        }
        return coor;
    }
    
    public Coordinates getPointCoordinates(DatasetInstance point, int width) {
        int y = 0;
        int x = point.getIndex() % width;
        for (int i = width; i <= point.getIndex(); i += width) {
            y++;
        }
        return new Coordinates(x, y);
    }
    
    private String getPoinsAsString() {
        String output = "";
        for (DatasetInstance point : points) {
            output += point.toString() + " ";
        }
        return output;
    }
    
    public void setEps(double eps) {
        this.eps = eps;
    }
    
    public double getEps() {
        return this.eps;
    }
    
    @Override
    public String toString() {
        return getPoinsAsString();
    }
    
}
