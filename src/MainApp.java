
import application.StopWatch;
import application.chart.ChartLine;
import application.chart.ChartPoint;
import data.DataReader;
import data.PictureUtils;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.SplittableRandom;
import javafx.application.Application;
import minig.clustering.DBSCAN.Dbscan;
import minig.clustering.GeneralCluster;
import minig.clustering.OPTICS.Optics;
import minig.clustering.kmeans.KMeans;
import minig.data.dataset.DataSet;
import minig.evaluation.CalinskyHarabaszIndex;
import minig.evaluation.DaviesBouldin;
import minig.evaluation.DunnIndex;
import minig.evaluation.I_Index;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Jaroslav
 */
public class MainApp {

    /*
        Choose one of these algorithms to run:
        1 -> k-menas
        2 -> DBSCAN
        3 -> OPTICS
    */
    public static int algorithm = 3;
    
    /*
    
    
    
    */
    
    public static DunnIndex dunnIndex;
    public static DaviesBouldin dbIndex;
    public static CalinskyHarabaszIndex chIndex;
    public static I_Index iIndex;
    
    public static ArrayList<GeneralCluster> clusters;
    
    public static int width = 900;
    public static int height = 600;
    public static String separator = " ";
    
    public static double eps = 0.02;
    public static int minPts = 100;
    public static int numOfClusers = 5;
    
    public static void main(String[] args) throws IOException {
    // public MainApp() throws IOException {
        DataSet data = readDataset();
        StopWatch sw = new StopWatch();
        
        dunnIndex = new DunnIndex(data);
        dbIndex = new DaviesBouldin(data);
        chIndex = new CalinskyHarabaszIndex(data);
        iIndex = new I_Index(data);

        BufferedImage image = PictureUtils.getOriginalImage(data, 2, 1, 0, 3, width, height);
        PictureUtils.showImage(image);

        switch(algorithm) {
            case 1:
                clusters = kmeansClustering(data);
                break;
            case 2:
                clusters = dbscanClustering(data);
                break;
            case 3:
                clusters = opticsClustering(data);
                break;
            default:
                System.out.println("Not implemented yet");
                break;
        }  
        
        System.out.println("**********");
        System.out.println("Evaluation: ");
        System.out.println("");
        
        System.out.println("Dunn index: " + dunnIndex.Measure(clusters));
        System.out.println("Davies Bouldin index: " + dbIndex.Measure(clusters));
        System.out.println("Calinsky Harabasz index: " + chIndex.Measure(clusters));
        System.out.println("I index: " + iIndex.Measure(clusters));
        
        System.out.println("");
        System.out.println("**********");
        System.out.println("");
        
        BufferedImage imageOfClusters = PictureUtils.getClustersImage(clusters, width, height);
        PictureUtils.showImage(imageOfClusters);
        
        System.out.println("Time: " + sw.getCurrentTime());
    }

    private static ArrayList<GeneralCluster> kmeansClustering(DataSet dataset) {
        KMeans scan = new KMeans(dataset, numOfClusers);
        scan.doClustering();
        scan.print();
        System.out.println(scan.getClusterCount());
        return scan.getGeneralClusterData();
    }
    
    private static ArrayList<GeneralCluster> dbscanClustering(DataSet dataset) {
        Dbscan scan = new Dbscan(dataset, minPts, eps);
        scan.doClustering();
//        scan.print();
        System.out.println(scan.getClusterCount());
        return scan.getGeneralClusterData();
    }
    
    private static ArrayList<GeneralCluster> opticsClustering(DataSet dataset) {
        Optics scan = new Optics(dataset, minPts, eps);
        scan.doClustering();
//        scan.print();

//        ChartLine.maxY = 0.008;
//        ChartLine.filter = 10;
//        ChartLine.distances = scan.getOrderedStructureOfDistances();
//        Application.launch(ChartLine.class);

//        ChartPoint.width = width;
//        ChartPoint.height = height;
//        ChartPoint.clusters = scan.getClusters();
//        Application.launch(ChartPoint.class);

        System.out.println(scan.getClusterCount());
        return scan.getClusters();
    }
    
    private static void expandDataSet(DataSet dataSet, int value) {
        SplittableRandom sp = new SplittableRandom(0);
        for (int i = 0; i < value; i++) {
            dataSet.addInstance(dataSet.getInstance(sp.nextInt(0, dataSet.getDataCount())));
        }
    }
    
    public static DataSet readDataset() throws IOException {
        String pathToFiles = "data";
        String[] fileNames = {
            "1-B2.dat",     // blue
            "2-B3.dat",     // green
            "3-B4.dat",     // red
            "4-B5.dat",
            "5-B6.dat",
            "6-B7.dat",
            "7-B8.dat",
            "8-B8A.dat",
            "9-B11.dat",
            "10-B12.dat"
        };
        
        DataSet data = DataReader.readDataset(pathToFiles, fileNames, separator);
        System.out.println("Number of loaded data: " + data.getDataCount());
        return data;
    }

}