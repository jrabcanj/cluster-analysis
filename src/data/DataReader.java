/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import minig.data.attribute.NumericAttr;
import minig.data.dataset.DataSet;

/**
 *
 * @author Jaroslav
 */
public class DataReader {
    
    public static NumericAttr readAttr(String filename, String separator) throws FileNotFoundException, IOException {
        NumericAttr attr = new NumericAttr(filename);
        BufferedReader br = new BufferedReader(new FileReader(new File(filename)));
        String line;
        while ((line = br.readLine()) != null) {
            String[] tokens = line.trim().split(separator);
            for (String token : tokens) {
                attr.addValue(token);
            }
        }
        return attr;
    }

    public static DataSet readDataset(String pathToFiles, String[] fileNames, String separator) throws IOException {
        DataSet pixels = new DataSet();
        pixels.setName("pixels");
        String path;
        for (String fileName : fileNames) {
            path = pathToFiles + "/" + fileName;
            NumericAttr attr = readAttr(path, separator);
            pixels.addAttribute(attr);
        }
        return pixels;
    }

    public static DataSet readDataset(File[] files, String separator) throws IOException {
        DataSet pixels = new DataSet();
        pixels.setName("pixels");
        for (File file : files) {
            NumericAttr attr = readAttr(file.getAbsolutePath(), separator);
            pixels.addAttribute(attr);
        }
        return pixels;
    }
    
}