/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.HeadlessException;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import minig.clustering.Coordinates;
import minig.clustering.GeneralCluster;
import minig.data.attribute.NumericAttr;
import minig.data.dataset.DataSet;
import minig.data.dataset.instances.DatasetInstance;
import minig.data.dataset.instances.Instance;

/**
 *
 * @author Jaroslav
 */
public class PictureUtils {

    public static BufferedImage getOriginalImage(DataSet dt, int r, int g, int b, int a, int width, int height) {
        List<Instance> instances = dt.getInstances();
        BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        int row = 0;
        int column = 0;
        for (Instance instance : instances) {
            float R = (float) instance.getValue(dt.<NumericAttr>getAttribute(r));
            float G = (float) instance.getValue(dt.<NumericAttr>getAttribute(g));
            float B = (float) instance.getValue(dt.<NumericAttr>getAttribute(b));
            float A = (float) instance.getValue(dt.<NumericAttr>getAttribute(a));
            bi.setRGB(column, row, new Color(R, G, B, A).getRGB());
            column++;
            if (column > width - 1) {
                column = 0;
                row++;
            }
        }
        return bi;
    }
    
    public static BufferedImage getClustersImage(ArrayList<GeneralCluster> clusters, int width, int height) {
        int x, y;
        float A = 1, R = 1, G = 1, B = 1;
        BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        // set white background
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                bi.setRGB(i, j, new Color(R, G, B, A).getRGB());
            }
        }
        // set clusters into colors
        for (GeneralCluster cluster : clusters) {
            if (cluster.getPoints().size() == 1) continue;
            R = (float) Math.random();
            G = (float) Math.random();
            B = (float) Math.random();
            A = (float) Math.random();
            for (DatasetInstance point : cluster.getPoints()) {
                Coordinates coordinates = cluster.getPointCoordinates(point, width);
                x = coordinates.getX() % width;
                y = coordinates.getY() % height;
                bi.setRGB(x, y, new Color(R, G, B, A).getRGB());
            }
            
            System.out.println("Cluster: " + Math.round(R*255)  + " " + Math.round(G*255) + " " + Math.round(B*255) + " count: " + cluster.getPoints().size());
        }
        return bi;
    }

    public static JFrame showImage(BufferedImage bi) throws HeadlessException {
        JFrame frame = new JFrame();
        frame.getContentPane().setLayout(new FlowLayout());
        frame.getContentPane().add(new JLabel(new ImageIcon(bi)));
        frame.pack();
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        return frame;
    }
}
